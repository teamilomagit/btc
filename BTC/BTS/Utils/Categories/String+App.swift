//
//  String+App.swift
//  BTS
//
//  Created by Pawan Ramteke on 23/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
extension String
{
    func trimString()->String
    {
       return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
}
