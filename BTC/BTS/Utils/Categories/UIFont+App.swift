//
//  UIFont+App.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
import UIKit

extension UIFont
{
    class func appRegularFont(size:CGFloat)->UIFont
    {
        return UIFont(name: APP_FONT_REGULAR, size: size)!
    }
    
    class func appBoldFont(size:CGFloat)->UIFont
    {
        return UIFont(name: APP_FONT_BOLD, size: size)!
    }
}
