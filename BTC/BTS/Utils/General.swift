//
//  General.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
import UIKit
let APP_FONT_REGULAR = "RobotoCondensed-Regular"
let APP_FONT_BOLD = "RobotoCondensed-Bold"
let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height

let VIEWMANAGER = ViewManager.shared


let DRAWER_SELECTION_NOTIFICATION  = "DRAWER_SELECTION_NOTIFICATION"

//Test
//let GOOGLE_MAP_KEY  =   "AIzaSyDbN9qXp_i3F5jSKK1aD-7L5kB1CcJqlT0"

//Live
//let GOOGLE_MAP_KEY  =   "AIzaSyCjlDhYN-i51F-Qj7JuJtYr1PhThL4gDhs"
let GOOGLE_MAP_KEY  =   "AIzaSyAfxZZUZtN68FCLfY3er4TWvw8ZiQV1hXA"

let APP_STORE_LINK  =   "https://itunes.apple.com/app/id1449936887"
//LIVE
var BASE_URL = "http://btconindia.com/btcservices/api.php?request="

//TEST
//var BASE_URL  = "http://btconindia.com/btcServices_test/api.php?request="

let LOGIN_API                =  "login"
let REGISTRATION_API         =  "register"
let GET_BTC_RATE_API         =  "getBTCRate"
let BTC_HISTORY_API          =  "btcHistory"
let MY_PROFILE_API           =  "myProfile"

//Forgot Password
let SEND_OTP_API             =  "send_OTP"
let VERIFY_OTP_API           =  "verify_OTP"
let RESET_PASSWORD_API       =  "resetPassword"

//Buy BTC & Sale BTC
let BUY_BTC_API             = "buyBTC"
let SELL_BTC_API            = "sellBTC"

//Transfer BTC
let CHECK_BTC_USER_API       =  "checkBTCuser"
let TRANSFER_BTC_API         =  "tranferBTC"

//Nearest Buyer - Seller List
let GET_NEAREST_SELLER_API   = "sellBTCList"
let GET_NEAREST_BUYER_API   = "buyBTCList"
let GET_BUYER_SELLER_DETAILS_API = "requestDetail"

//Account Statement
let GET_ACCOUNT_STATEMENT_API = "statementBTC"
let GET_ACCOUNT_STATEMENT_DETAILS_API   = "transDetail"
//Profile
let UPDATE_PROFILE_API         = "updateProfile"
let CHANGE_PROFILE_PIC_API     = "profileimage"

//My Wallet
let MY_BTC_REQUEST_API          =   "userBtcRequest"
let DELETE_BTC_REQUEST_API      =   "deleteBtcRequest"

let ABOUT_BTC_URL              = "http://www.btconindia.com/btcservices/about_app.html"
let PRIVACY_POLICY_URL         = "http://www.btconindia.com/btcservices/privacy_policy.html"
let TERMS_AND_CONDITION_URL    = "http://www.btconindia.com/btcservices/term_conditions.html"
let CUSTOMER_SUPPORT_URL       = "http://www.btconindia.com/btcservices/customer_support.html"

