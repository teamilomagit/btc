//
//  ViewManager.swift
//  BTS
//
//  Created by Pawan Ramteke on 23/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
import Toast_Swift
import SVProgressHUD
import KYDrawerController
class ViewManager : NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    static let shared = ViewManager()
    var currentUser = LoginModel()
    var rateModel : BTCRateModel!
    
    var imagePickerBlock : ((UIImage,String)->())?
    private override init(){
        
    }
    
    func showToast(message:String)
    {
        ToastManager.shared.style.messageFont = UIFont.appRegularFont(size: 18)
        UIApplication.shared.keyWindow?.makeToast(message)
    }
    
    func showActivityIndicator()
    {
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setRingThickness(2)
        SVProgressHUD.show()
    }
    
    func hideActivityIndicator()
    {
        SVProgressHUD.dismiss()
    }
    
    func getBTCRateAPICall(completion: @escaping () -> Void)
    {
        let param : [String : AnyObject] = [
            "user_id":VIEWMANAGER.currentUser.bTCSubrUserID as AnyObject
        ]
       // self.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_BTC_RATE_API, params: param, completion: { (responseData) in
       //         self.hideActivityIndicator()
                self.rateModel = responseData as! BTCRateModel
                completion()
            
        }) { (errMsg) in
        //    self.hideActivityIndicator()
          //  self.showToast(message: errMsg!)
        }
    }
    
    func topMostController() -> UIViewController
    {
        var topViewController = UIApplication.shared.keyWindow?.rootViewController
        while true {
            if topViewController?.presentedViewController != nil {
                topViewController = topViewController?.presentedViewController
            }
            else if (topViewController is KYDrawerController) {
                let kyDrawer = topViewController as? KYDrawerController
                if (kyDrawer?.mainViewController is UINavigationController)
                {
                    let vc = kyDrawer?.mainViewController as! UINavigationController
                    return vc.topViewController!
                }
                return (kyDrawer?.mainViewController)!
            }
            else if (topViewController is UINavigationController) {
                let nav = topViewController as? UINavigationController
                if (nav?.topViewController is KYDrawerController) {
                    let kyDrawer = nav?.topViewController as? KYDrawerController
                    return (kyDrawer?.mainViewController)!
                }
                topViewController = nav?.topViewController
            }
            else if (topViewController is UITabBarController) {
                let tab = topViewController as? UITabBarController
                topViewController = tab?.selectedViewController
                break
            }
            else {
                break
            }
        }
        return topViewController!
    }
    
    func showImagePicker(block : @escaping (UIImage,String) -> Void)
    {
        imagePickerBlock = block
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.showImagePicker(sourceType: .camera)
        }
        
        let gallery = UIAlertAction(title: "Gallery", style: .default) { (action) in
            self.showImagePicker(sourceType: .photoLibrary)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        self.topMostController().present(actionSheet, animated: true, completion: nil)
    }
    
    func showImagePicker(sourceType : UIImagePickerControllerSourceType)
    {
        let imgPicker =  UIImagePickerController()
        imgPicker.sourceType = sourceType
        imgPicker.delegate = self
        self.topMostController().present(imgPicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        do{
            let paths = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask)
            let documentsDirectory : URL = paths[0]
            let localPath = documentsDirectory.appendingPathComponent("Profile_pic.png")
            
            let data = UIImageJPEGRepresentation(image, 0.5)
            

            try data?.write(to: localPath)
            
            if self.imagePickerBlock != nil {
                self.imagePickerBlock!(image,localPath.path)
            }
        }
        catch{
            
        }
        self.topMostController().dismiss(animated: true, completion: nil)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
