//
//  Preferences.swift
//  BTS
//
//  Created by Pawan Ramteke on 23/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
import UIKit
let USER_DEFAULTS  = UserDefaults.standard
class Preferences
{
    class func saveLoginData(response:NSDictionary)
    {
        USER_DEFAULTS.setValue(response, forKey: "saveLoginData")
    }
    
    class func getLoginData() -> Any?
    {
        return USER_DEFAULTS.value(forKey: "saveLoginData")
    }
    
    class func clearLoginData()
    {
        USER_DEFAULTS.removeObject(forKey: "saveLoginData")
    }
}
