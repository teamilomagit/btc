//
//  BTCHistoryModel.swift
//  BTS
//
//  Created by Pawan Ramteke on 26/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
class BTCHistoryModel
{
    var btcRate : String?
    var date : String?
    
    init() {
        
    }
    class func modelParserWithDictionary(json:[String : Any]) -> BTCHistoryModel
    {
        let model = BTCHistoryModel()
        model.btcRate = json["btcRate"] as? String ?? "-"
        model.date = json["date"] as? String
        return model
    }
    
}
