//
//  UserModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 27, 2018

import Foundation


class UserModel : NSObject, NSCoding{

    var sCity : String!
    var sName : String!
    var sPhone : String!
    var subID : String!

    override init() {
        
    }
    
    class func modelParserWithDictionary(dictionary:[String : Any]) -> UserModel
    {
        let model = UserModel()
        
        model.sCity = dictionary["sCity"] as? String
        model.sName = dictionary["sName"] as? String
        model.sPhone = dictionary["sPhone"] as? String
        model.subID = dictionary["subID"] as? String
        
        return model
    }
    

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if sCity != nil{
            dictionary["sCity"] = sCity
        }
        if sName != nil{
            dictionary["sName"] = sName
        }
        if sPhone != nil{
            dictionary["sPhone"] = sPhone
        }
        if subID != nil{
            dictionary["subID"] = subID
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        sCity = aDecoder.decodeObject(forKey: "sCity") as? String
        sName = aDecoder.decodeObject(forKey: "sName") as? String
        sPhone = aDecoder.decodeObject(forKey: "sPhone") as? String
        subID = aDecoder.decodeObject(forKey: "subID") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if sCity != nil{
            aCoder.encode(sCity, forKey: "sCity")
        }
        if sName != nil{
            aCoder.encode(sName, forKey: "sName")
        }
        if sPhone != nil{
            aCoder.encode(sPhone, forKey: "sPhone")
        }
        if subID != nil{
            aCoder.encode(subID, forKey: "subID")
        }
    }
}
