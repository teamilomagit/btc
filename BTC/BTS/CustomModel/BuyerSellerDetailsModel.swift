//
//  BuyerSellerDetailsModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 31, 2018

import Foundation


class BuyerSellerDetailsModel : NSObject, NSCoding{

    var address : String!
    var bTC : String!
    var bTCSubrNo : String!
    var fName : String!
    var lat : String!
    var lName : String!
    var lng : String!
    var mobile : String!
    var profilePic : String!
    var rDate : String!


    override init() {
        
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    class func modelParserWithDictionary(dictionary:[String : Any]) -> BuyerSellerDetailsModel
    {
        let model = BuyerSellerDetailsModel()
        
        model.address = dictionary["address"] as? String
        model.bTC = dictionary["BTC"] as? String
        model.bTCSubrNo = dictionary["BTC_subr_no"] as? String
        model.fName = dictionary["fName"] as? String
        model.lat = dictionary["lat"] as? String
        model.lName = dictionary["lName"] as? String
        model.lng = dictionary["lng"] as? String
        model.mobile = dictionary["mobile"] as? String
        model.profilePic = dictionary["profilePic"] as? String
        model.rDate = dictionary["rDate"] as? String
        return model
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if bTC != nil{
            dictionary["BTC"] = bTC
        }
        if bTCSubrNo != nil{
            dictionary["BTC_subr_no"] = bTCSubrNo
        }
        if fName != nil{
            dictionary["fName"] = fName
        }
        if lat != nil{
            dictionary["lat"] = lat
        }
        if lName != nil{
            dictionary["lName"] = lName
        }
        if lng != nil{
            dictionary["lng"] = lng
        }
        if mobile != nil{
            dictionary["mobile"] = mobile
        }
        if profilePic != nil{
            dictionary["profilePic"] = profilePic
        }
        if rDate != nil{
            dictionary["rDate"] = rDate
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        bTC = aDecoder.decodeObject(forKey: "BTC") as? String
        bTCSubrNo = aDecoder.decodeObject(forKey: "BTC_subr_no") as? String
        fName = aDecoder.decodeObject(forKey: "fName") as? String
        lat = aDecoder.decodeObject(forKey: "lat") as? String
        lName = aDecoder.decodeObject(forKey: "lName") as? String
        lng = aDecoder.decodeObject(forKey: "lng") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        profilePic = aDecoder.decodeObject(forKey: "profilePic") as? String
        rDate = aDecoder.decodeObject(forKey: "rDate") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if bTC != nil{
            aCoder.encode(bTC, forKey: "BTC")
        }
        if bTCSubrNo != nil{
            aCoder.encode(bTCSubrNo, forKey: "BTC_subr_no")
        }
        if fName != nil{
            aCoder.encode(fName, forKey: "fName")
        }
        if lat != nil{
            aCoder.encode(lat, forKey: "lat")
        }
        if lName != nil{
            aCoder.encode(lName, forKey: "lName")
        }
        if lng != nil{
            aCoder.encode(lng, forKey: "lng")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if profilePic != nil{
            aCoder.encode(profilePic, forKey: "profilePic")
        }
        if rDate != nil{
            aCoder.encode(rDate, forKey: "rDate")
        }
    }
}
