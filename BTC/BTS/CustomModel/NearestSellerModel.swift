//
//  NearestBuyerModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 28, 2018

import Foundation


class NearestSellerModel : NSObject, NSCoding{

    var sDate : String!
    var sLat : String!
    var sLng : String!
    var sName : String!
    var sReqId : String!
    var bTC : String!
    var distance : String!

    override init() {
        
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    class func modelParserWithDictionary(dictionary:[String : Any]) -> NearestSellerModel
    {
        let model = NearestSellerModel()
    
        model.sDate = dictionary["sDate"] as? String
        model.sLat = dictionary["sLat"] as? String
        model.sLng = dictionary["sLng"] as? String
        model.sName = dictionary["sName"] as? String
        model.sReqId = dictionary["sReqId"] as? String
        model.bTC = dictionary["BTC"] as? String
        model.distance = "\(dictionary["distance"]!)"

    return model
    
    }


    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if sDate != nil{
            dictionary["sDate"] = sDate
        }
        if sLat != nil{
            dictionary["sLat"] = sLat
        }
        if sLng != nil{
            dictionary["sLng"] = sLng
        }
        if sName != nil{
            dictionary["sName"] = sName
        }
        if sReqId != nil{
            dictionary["sReqId"] = sReqId
        }
        if bTC != nil{
            dictionary["BTC"] = bTC
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        sDate = aDecoder.decodeObject(forKey: "sDate") as? String
        sLat = aDecoder.decodeObject(forKey: "sLat") as? String
        sLng = aDecoder.decodeObject(forKey: "sLng") as? String
        sName = aDecoder.decodeObject(forKey: "sName") as? String
        sReqId = aDecoder.decodeObject(forKey: "sReqId") as? String
        bTC = aDecoder.decodeObject(forKey: "BTC") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if sDate != nil{
            aCoder.encode(sDate, forKey: "sDate")
        }
        if sLat != nil{
            aCoder.encode(sLat, forKey: "sLat")
        }
        if sLng != nil{
            aCoder.encode(sLng, forKey: "sLng")
        }
        if sName != nil{
            aCoder.encode(sName, forKey: "sName")
        }
        if sReqId != nil{
            aCoder.encode(sReqId, forKey: "sReqId")
        }
        if bTC != nil{
            aCoder.encode(bTC, forKey: "BTC")
        }
    }
}
