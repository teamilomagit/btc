//
//  MyBTCRequestModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 25, 2018

import Foundation


class MyBTCRequestModel : NSObject, NSCoding{

    var rBTC : String!
    var rDate : String!
    var reqId : String!
    var rType : String!


    override init() {
        
    }
    
    class func modelParserWithDictionary(dictionary:[String : Any]) -> MyBTCRequestModel
    {
        let model = MyBTCRequestModel()
        model.rBTC = dictionary["rBTC"] as? String
        model.rDate = dictionary["rDate"] as? String
        model.reqId = dictionary["req_id"] as? String
        model.rType = dictionary["rType"] as? String
        
        return model
    }
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if rBTC != nil{
            dictionary["rBTC"] = rBTC
        }
        if rDate != nil{
            dictionary["rDate"] = rDate
        }
        if reqId != nil{
            dictionary["req_id"] = reqId
        }
        if rType != nil{
            dictionary["rType"] = rType
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        rBTC = aDecoder.decodeObject(forKey: "rBTC") as? String
        rDate = aDecoder.decodeObject(forKey: "rDate") as? String
        reqId = aDecoder.decodeObject(forKey: "req_id") as? String
        rType = aDecoder.decodeObject(forKey: "rType") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if rBTC != nil{
            aCoder.encode(rBTC, forKey: "rBTC")
        }
        if rDate != nil{
            aCoder.encode(rDate, forKey: "rDate")
        }
        if reqId != nil{
            aCoder.encode(reqId, forKey: "req_id")
        }
        if rType != nil{
            aCoder.encode(rType, forKey: "rType")
        }
    }
}
