//
//  LoginModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 26, 2018

import Foundation


class LoginModel : NSObject, NSCoding{

    var addressLine : String!
    var bTCAccBal : Int!
    var bTCStatus : String!
    var bTCSubrNo : String!
    var bTCSubrUserID : String!
    var bTCWalletID : String!
    var city : String!
    var dob : String!
    var eMail : String!
    var fName : String!
    var gender : String!
    var incomeRange : String!
    var landmark : String!
    var lName : String!
    var mobile : String!
    var occupation : String!
    var pincode : String!
    var profilePic : String!
    var state : String!
    var qrCode : String!

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    
    override init() {
        
    }
    class func modelParserWithDictionary(dictionary:[String : Any]) -> LoginModel
    {
        let model = LoginModel()
    
        model.addressLine = dictionary["address_line"] as? String
        model.bTCAccBal = dictionary["BTC_accBal"] as? Int
        model.bTCStatus = dictionary["BTC_status"] as? String
        model.bTCSubrNo = dictionary["BTC_subr_no"] as? String
        model.bTCSubrUserID = dictionary["BTC_subr_userID"] as? String
        model.bTCWalletID = dictionary["BTC_walletID"] as? String
        model.city = dictionary["city"] as? String
        model.dob = dictionary["dob"] as? String
        model.eMail = dictionary["eMail"] as? String
        model.fName = dictionary["fName"] as? String
        model.gender = dictionary["gender"] as? String
        model.incomeRange = dictionary["incomeRange"] as? String
        model.landmark = dictionary["landmark"] as? String
        model.lName = dictionary["lName"] as? String
        model.mobile = dictionary["mobile"] as? String
        model.occupation = dictionary["occupation"] as? String
        model.pincode = dictionary["pincode"] as? String
        model.profilePic = dictionary["profilePic"] as? String
        model.state = dictionary["state"] as? String
        model.qrCode = dictionary["qrCode"] as? String
        return model
    }
    
   
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if addressLine != nil{
            dictionary["address_line"] = addressLine
        }
        if bTCAccBal != nil{
            dictionary["BTC_accBal"] = bTCAccBal
        }
        if bTCStatus != nil{
            dictionary["BTC_status"] = bTCStatus
        }
        if bTCSubrNo != nil{
            dictionary["BTC_subr_no"] = bTCSubrNo
        }
        if bTCSubrUserID != nil{
            dictionary["BTC_subr_userID"] = bTCSubrUserID
        }
        if bTCWalletID != nil{
            dictionary["BTC_walletID"] = bTCWalletID
        }
        if city != nil{
            dictionary["city"] = city
        }
        if dob != nil{
            dictionary["dob"] = dob
        }
        if eMail != nil{
            dictionary["eMail"] = eMail
        }
        if fName != nil{
            dictionary["fName"] = fName
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if incomeRange != nil{
            dictionary["incomeRange"] = incomeRange
        }
        if landmark != nil{
            dictionary["landmark"] = landmark
        }
        if lName != nil{
            dictionary["lName"] = lName
        }
        if mobile != nil{
            dictionary["mobile"] = mobile
        }
        if occupation != nil{
            dictionary["occupation"] = occupation
        }
        if pincode != nil{
            dictionary["pincode"] = pincode
        }
        if profilePic != nil{
            dictionary["profilePic"] = profilePic
        }
        if state != nil{
            dictionary["state"] = state
        }
        if qrCode != nil {
            dictionary["qrCode"] = qrCode
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        addressLine = aDecoder.decodeObject(forKey: "address_line") as? String
        bTCAccBal = aDecoder.decodeObject(forKey: "BTC_accBal") as? Int
        bTCStatus = aDecoder.decodeObject(forKey: "BTC_status") as? String
        bTCSubrNo = aDecoder.decodeObject(forKey: "BTC_subr_no") as? String
        bTCSubrUserID = aDecoder.decodeObject(forKey: "BTC_subr_userID") as? String
        bTCWalletID = aDecoder.decodeObject(forKey: "BTC_walletID") as? String
        city = aDecoder.decodeObject(forKey: "city") as? String
        dob = aDecoder.decodeObject(forKey: "dob") as? String
        eMail = aDecoder.decodeObject(forKey: "eMail") as? String
        fName = aDecoder.decodeObject(forKey: "fName") as? String
        gender = aDecoder.decodeObject(forKey: "gender") as? String
        incomeRange = aDecoder.decodeObject(forKey: "incomeRange") as? String
        landmark = aDecoder.decodeObject(forKey: "landmark") as? String
        lName = aDecoder.decodeObject(forKey: "lName") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        occupation = aDecoder.decodeObject(forKey: "occupation") as? String
        pincode = aDecoder.decodeObject(forKey: "pincode") as? String
        profilePic = aDecoder.decodeObject(forKey: "profilePic") as? String
        state = aDecoder.decodeObject(forKey: "state") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if addressLine != nil{
            aCoder.encode(addressLine, forKey: "address_line")
        }
        if bTCAccBal != nil{
            aCoder.encode(bTCAccBal, forKey: "BTC_accBal")
        }
        if bTCStatus != nil{
            aCoder.encode(bTCStatus, forKey: "BTC_status")
        }
        if bTCSubrNo != nil{
            aCoder.encode(bTCSubrNo, forKey: "BTC_subr_no")
        }
        if bTCSubrUserID != nil{
            aCoder.encode(bTCSubrUserID, forKey: "BTC_subr_userID")
        }
        if bTCWalletID != nil{
            aCoder.encode(bTCWalletID, forKey: "BTC_walletID")
        }
        if city != nil{
            aCoder.encode(city, forKey: "city")
        }
        if dob != nil{
            aCoder.encode(dob, forKey: "dob")
        }
        if eMail != nil{
            aCoder.encode(eMail, forKey: "eMail")
        }
        if fName != nil{
            aCoder.encode(fName, forKey: "fName")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "gender")
        }
        if incomeRange != nil{
            aCoder.encode(incomeRange, forKey: "incomeRange")
        }
        if landmark != nil{
            aCoder.encode(landmark, forKey: "landmark")
        }
        if lName != nil{
            aCoder.encode(lName, forKey: "lName")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if occupation != nil{
            aCoder.encode(occupation, forKey: "occupation")
        }
        if pincode != nil{
            aCoder.encode(pincode, forKey: "pincode")
        }
        if profilePic != nil{
            aCoder.encode(profilePic, forKey: "profilePic")
        }
        if state != nil{
            aCoder.encode(state, forKey: "state")
        }
    }
}
