//
//  NearestBuyerModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 28, 2018

import Foundation


class NearestBuyerModel : NSObject, NSCoding{

    var bDate : String!
    var bLat : String!
    var bLng : String!
    var bName : String!
    var bReqId : String!
    var bTC : String!
    var distance : String!
    override init() {
        
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    class func modelParserWithDictionary(dictionary:[String : Any]) -> NearestBuyerModel
    {
        let model = NearestBuyerModel()
    
        model.bDate = dictionary["bDate"] as? String
        model.bLat = dictionary["bLat"] as? String
        model.bLng = dictionary["bLng"] as? String
        model.bName = dictionary["bName"] as? String
        model.bReqId = dictionary["bReqId"] as? String
        model.bTC = dictionary["BTC"] as? String
        model.distance = "\(dictionary["distance"]!)"
        return model
    
    }


    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if bDate != nil{
            dictionary["bDate"] = bDate
        }
        if bLat != nil{
            dictionary["bLat"] = bLat
        }
        if bLng != nil{
            dictionary["bLng"] = bLng
        }
        if bName != nil{
            dictionary["bName"] = bName
        }
        if bReqId != nil{
            dictionary["bReqId"] = bReqId
        }
        if bTC != nil{
            dictionary["BTC"] = bTC
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        bDate = aDecoder.decodeObject(forKey: "bDate") as? String
        bLat = aDecoder.decodeObject(forKey: "bLat") as? String
        bLng = aDecoder.decodeObject(forKey: "bLng") as? String
        bName = aDecoder.decodeObject(forKey: "bName") as? String
        bReqId = aDecoder.decodeObject(forKey: "bReqId") as? String
        bTC = aDecoder.decodeObject(forKey: "BTC") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if bDate != nil{
            aCoder.encode(bDate, forKey: "bDate")
        }
        if bLat != nil{
            aCoder.encode(bLat, forKey: "bLat")
        }
        if bLng != nil{
            aCoder.encode(bLng, forKey: "bLng")
        }
        if bName != nil{
            aCoder.encode(bName, forKey: "bName")
        }
        if bReqId != nil{
            aCoder.encode(bReqId, forKey: "bReqId")
        }
        if bTC != nil{
            aCoder.encode(bTC, forKey: "BTC")
        }
    }
}
