//
//  UserDetailsModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 31, 2018

import Foundation


class AccStmtDetailsModel : NSObject, NSCoding{

    var address : String!
    var bTCSubrNo : String!
    var fName : String!
    var lName : String!
    var mobile : String!
    var profilePic : String!
    var tBalance : String!
    var tBTC : String!
    var tDate : String!
    var tType : String!
    

    override init() {
        
    }
    
    class func modelParserWithDictionary(dictionary:[String : Any]) -> AccStmtDetailsModel
    {
        let model = AccStmtDetailsModel()
        model.address = dictionary["address"] as? String
        model.bTCSubrNo = dictionary["BTC_subr_no"] as? String
        model.fName = dictionary["fName"] as? String
        model.lName = dictionary["lName"] as? String
        model.mobile = dictionary["mobile"] as? String
        model.profilePic = dictionary["profilePic"] as? String
        model.tBalance = dictionary["tBalance"] as? String
        model.tBTC = dictionary["tBTC"] as? String
        model.tDate = dictionary["tDate"] as? String
        model.tType = dictionary["tType"] as? String
    
        return model
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if bTCSubrNo != nil{
            dictionary["BTC_subr_no"] = bTCSubrNo
        }
        if fName != nil{
            dictionary["fName"] = fName
        }
        if lName != nil{
            dictionary["lName"] = lName
        }
        if mobile != nil{
            dictionary["mobile"] = mobile
        }
        if profilePic != nil{
            dictionary["profilePic"] = profilePic
        }
        if tBalance != nil{
            dictionary["tBalance"] = tBalance
        }
        if tBTC != nil{
            dictionary["tBTC"] = tBTC
        }
        if tDate != nil{
            dictionary["tDate"] = tDate
        }
        if tType != nil{
            dictionary["tType"] = tType
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        bTCSubrNo = aDecoder.decodeObject(forKey: "BTC_subr_no") as? String
        fName = aDecoder.decodeObject(forKey: "fName") as? String
        lName = aDecoder.decodeObject(forKey: "lName") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        profilePic = aDecoder.decodeObject(forKey: "profilePic") as? String
        tBalance = aDecoder.decodeObject(forKey: "tBalance") as? String
        tBTC = aDecoder.decodeObject(forKey: "tBTC") as? String
        tDate = aDecoder.decodeObject(forKey: "tDate") as? String
        tType = aDecoder.decodeObject(forKey: "tType") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if bTCSubrNo != nil{
            aCoder.encode(bTCSubrNo, forKey: "BTC_subr_no")
        }
        if fName != nil{
            aCoder.encode(fName, forKey: "fName")
        }
        if lName != nil{
            aCoder.encode(lName, forKey: "lName")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if profilePic != nil{
            aCoder.encode(profilePic, forKey: "profilePic")
        }
        if tBalance != nil{
            aCoder.encode(tBalance, forKey: "tBalance")
        }
        if tBTC != nil{
            aCoder.encode(tBTC, forKey: "tBTC")
        }
        if tDate != nil{
            aCoder.encode(tDate, forKey: "tDate")
        }
        if tType != nil{
            aCoder.encode(tType, forKey: "tType")
        }
    }
}
