//
//  MyAccStmtModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on December 27, 2018

import Foundation


class MyAccStmtModel : NSObject, NSCoding{

    var tBalance : String!
    var tBTC : String!
    var tDate : String!
    var tType : String!
    var walletID : String!
    var transID : String!
    
    override init() {
        
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    class func modelParserWithDictionary(dictionary:[String : Any]) -> MyAccStmtModel
    {
        let model = MyAccStmtModel()
        model.tBalance = dictionary["tBalance"] as? String
        model.tBTC = dictionary["tBTC"] as? String
        model.tDate = dictionary["tDate"] as? String
        model.tType = dictionary["tType"] as? String
        model.walletID = dictionary["walletID"] as? String
        model.transID = dictionary["transID"] as? String
        return model
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if tBalance != nil{
            dictionary["tBalance"] = tBalance
        }
        if tBTC != nil{
            dictionary["tBTC"] = tBTC
        }
        if tDate != nil{
            dictionary["tDate"] = tDate
        }
        if tType != nil{
            dictionary["tType"] = tType
        }
        if walletID != nil{
            dictionary["walletID"] = walletID
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        tBalance = aDecoder.decodeObject(forKey: "tBalance") as? String
        tBTC = aDecoder.decodeObject(forKey: "tBTC") as? String
        tDate = aDecoder.decodeObject(forKey: "tDate") as? String
        tType = aDecoder.decodeObject(forKey: "tType") as? String
        walletID = aDecoder.decodeObject(forKey: "walletID") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if tBalance != nil{
            aCoder.encode(tBalance, forKey: "tBalance")
        }
        if tBTC != nil{
            aCoder.encode(tBTC, forKey: "tBTC")
        }
        if tDate != nil{
            aCoder.encode(tDate, forKey: "tDate")
        }
        if tType != nil{
            aCoder.encode(tType, forKey: "tType")
        }
        if walletID != nil{
            aCoder.encode(walletID, forKey: "walletID")
        }
    }
}
