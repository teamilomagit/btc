//
//  BTCRateModel.swift
//  BTS
//
//  Created by Pawan Ramteke on 23/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
class BTCRateModel
{
    var btcAccBalance : String?
    var btcRate : String?
    var date : String?
    
    init() {
        
    }
    class func modelParserWithDictionary(json:[String : Any]) -> BTCRateModel
    {
        let model = BTCRateModel()
        model.btcAccBalance = json["BTC_accBal"] as? String
        model.btcRate = json["btcRate"] as? String ?? "-"
        model.date = json["date"] as? String
        return model
    }
    
}
