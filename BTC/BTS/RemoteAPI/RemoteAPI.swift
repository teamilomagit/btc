
//
//  RemoteAPI.swift
//  BTS
//
//  Created by Pawan Ramteke on 22/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class RemoteAPI{
    
    func callPOSTApi(apiName:String,params:[String: AnyObject],completion: @escaping (AnyObject) -> Void,failed:@escaping (_ errorMsg:String?) -> Void)
    {
        let fullAPI = "\(BASE_URL)\(apiName)"
        Alamofire.request(fullAPI, method: .post, parameters: params, encoding: JSONEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
            }
            .validate ()
            .responseJSON { response in
                debugPrint(response)
                if let ERR = response.result.error
                {
                    print(ERR);
                    failed(ERR.localizedDescription)
                    return
                }
                
                if let JSON = response.result.value
                {
                    let serverResponse = JSON as? NSDictionary
                    if serverResponse?.value(forKey: "success") as! Bool == false
                    {
                        failed(serverResponse?.value(forKey: "msg") as? String)
                        return
                    }
                    
                    if apiName == UPDATE_PROFILE_API || apiName == TRANSFER_BTC_API || apiName == SEND_OTP_API || apiName == VERIFY_OTP_API || apiName == RESET_PASSWORD_API || apiName == BUY_BTC_API || apiName == SELL_BTC_API ||  apiName == DELETE_BTC_REQUEST_API {
                        completion(serverResponse?.value(forKey: "msg") as AnyObject)
                        return
                    }
                    
                    let obj = ModelParser().modelParserWithData(apiURL: apiName, responseData: serverResponse?.value(forKey: "response") as AnyObject)
                    completion(obj)
                }
        }
    }
    
    func uploadProfileImage(apiName:String,params:[String: AnyObject],completion: @escaping (AnyObject) -> Void,failed:@escaping (_ errorMsg:String?) -> Void)
    {
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            ]
        let fullAPI = "\(BASE_URL)\(apiName)"

        let url = URL(string: fullAPI)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"

        Alamofire.upload(multipartFormData: { (multipartFormData) in
         //   multipartFormData.append((params["profImgfile"] as! String).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: "profImgfile")
            multipartFormData.append((params["user_id"] as! String).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: "user_id")
            
            let fileUrl = URL(fileURLWithPath:params["profImgfile"] as! String)
            multipartFormData.append(fileUrl, withName: "profImgfile")

            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (encodingResult) in
            switch encodingResult
            {
                case .success(let upload, _, _):
                    upload.responseJSON(completionHandler: { (response) in
                        if let serverResponse = response.result.value as? NSDictionary {
                            if serverResponse.value(forKey: "success") as! Bool == false
                            {
                                failed(serverResponse.value(forKey: "msg") as? String)
                                return
                            }
                            completion(serverResponse.value(forKey: "profilePic") as AnyObject)
                        }
                        else{
                            failed("Something is wrong")
                            return

                        }
                    })
                break

                case .failure(let error):
                    failed(error.localizedDescription)
                break
        }
        }
    }
}

