//
//  ModelParser.swift
//  BTS
//
//  Created by Pawan Ramteke on 22/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
import UIKit
class ModelParser {
    
    func modelParserWithData(apiURL:String,responseData:AnyObject)->AnyObject
    {
        if apiURL == LOGIN_API || apiURL == REGISTRATION_API {
            return parseLoginData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_BTC_RATE_API {
            return parseBTCRateData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == BTC_HISTORY_API {
            return parseBTCHistoryData(responseData: responseData as! NSArray)
        }
        if apiURL == MY_PROFILE_API {
            return parseMyProfileData(responseData: responseData as! NSDictionary)
        }
        if apiURL == CHECK_BTC_USER_API {
            return parseBTCUserData(responseData: responseData as! NSDictionary)
        }
        if apiURL == GET_ACCOUNT_STATEMENT_API {
            return parseAccountStmtData(responseData: responseData as! NSArray)
        }
        if apiURL == MY_BTC_REQUEST_API {
            return parseMyBTCRequestData(responseData: responseData as! NSArray)
        }
        if apiURL == GET_NEAREST_SELLER_API {
            return parseNearestSellerData(responseData: responseData as! NSArray)
        }
        if apiURL == GET_NEAREST_BUYER_API {
            return parseNearestBuyerData(responseData: responseData as! NSArray)
        }
        
        if apiURL == GET_BUYER_SELLER_DETAILS_API {
            return parseBuyerSellerDetailsData(responseData: responseData as! NSDictionary)
        }
        if apiURL == GET_ACCOUNT_STATEMENT_DETAILS_API {
            return parseAccStmtDetailsData(responseData: responseData as! NSDictionary)
        }
        return [] as AnyObject
    }
    
    func parseLoginData(responseData:NSDictionary)->LoginModel
    {
        Preferences.saveLoginData(response: responseData)
        return LoginModel.modelParserWithDictionary(dictionary: responseData as! [String : Any])
    }
    
    func parseBTCRateData(responseData:NSDictionary)->BTCRateModel
    {
        return BTCRateModel.modelParserWithDictionary(json: responseData as! [String : Any])
    }
    
    func parseBTCHistoryData(responseData:NSArray)->NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = BTCHistoryModel.modelParserWithDictionary(json: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseMyProfileData(responseData:NSDictionary) -> MyProfileModel
    {
        Preferences.saveLoginData(response: responseData)
        VIEWMANAGER.currentUser = LoginModel.modelParserWithDictionary(dictionary: responseData as! [String : Any])
        return MyProfileModel.modelParserWithDictionary(dictionary: responseData as! [String : Any])
    }
    
    func parseBTCUserData(responseData:NSDictionary) -> UserModel
    {
        return UserModel.modelParserWithDictionary(dictionary: responseData as! [String : Any])
    }
    
    func parseAccountStmtData(responseData:NSArray)->NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = MyAccStmtModel.modelParserWithDictionary(dictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseMyBTCRequestData(responseData:NSArray)->NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = MyBTCRequestModel.modelParserWithDictionary(dictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseNearestBuyerData(responseData:NSArray)->NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = NearestBuyerModel.modelParserWithDictionary(dictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseNearestSellerData(responseData:NSArray)->NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = NearestSellerModel.modelParserWithDictionary(dictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseAccStmtDetailsData(responseData:NSDictionary) -> AccStmtDetailsModel
    {
        return AccStmtDetailsModel.modelParserWithDictionary(dictionary: responseData as! [String : Any])
    }
    
    func parseBuyerSellerDetailsData(responseData:NSDictionary) -> BuyerSellerDetailsModel
    {
        return BuyerSellerDetailsModel.modelParserWithDictionary(dictionary: responseData as! [String : Any])
    }
}
