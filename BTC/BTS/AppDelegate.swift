//
//  AppDelegate.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import KYDrawerController
import GoogleMaps
import GooglePlaces
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        initializeeGoogleMap()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarTintColor = UIColor.appThemeColor
        window = UIWindow(frame: UIScreen.main.bounds)
      //  setStatusBarBackgroundColor(color: UIColor.appThemeColor)
        setSplashAsRoot()

        return true
    }
    
    func initializeeGoogleMap()
    {
        GMSServices.provideAPIKey(GOOGLE_MAP_KEY)
        GMSPlacesClient.provideAPIKey(GOOGLE_MAP_KEY)
    }
    
    func setSplashAsRoot()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let signupVC = storyboard.instantiateViewController(withIdentifier: "SplashViewController")
        let navVC = UINavigationController(rootViewController: signupVC)
        self.window?.rootViewController = navVC;
        window?.makeKeyAndVisible()
    }
    
    func checkForLogin()
    {
        if Preferences.getLoginData() == nil {
            setRegistrationAsRoot()
        }
        else{
            let loginModel = LoginModel.modelParserWithDictionary(dictionary: Preferences.getLoginData() as! [String : Any])
            VIEWMANAGER.currentUser = loginModel
            setDashboardAsRoot()
        }
    }
    
    func setRegistrationAsRoot()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let signupVC = storyboard.instantiateViewController(withIdentifier: "SignupViewController")
        let navVC = UINavigationController(rootViewController: signupVC)
        
        UIView.animate(withDuration: 2.0, delay: 0.0, options: .transitionCrossDissolve, animations: {
            self.window?.rootViewController = navVC;
        }, completion: nil)
        window?.makeKeyAndVisible()
    }
    
    func setDashboardAsRoot()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let dashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardController")

        let drawerViewController = storyboard.instantiateViewController(withIdentifier: "DrawerViewController")
        
        let drawerController = KYDrawerController(drawerDirection: KYDrawerController.DrawerDirection.left, drawerWidth: SCREEN_WIDTH * 0.8)
        drawerController.mainViewController = UINavigationController(rootViewController: dashboardVC)
        drawerController.drawerViewController = drawerViewController
        
        /* Customize
         drawerController.drawerDirection = .Right
         drawerController.drawerWidth     = 200
         */
        UIView.animate(withDuration: 2.0, delay: 0.0, options: .transitionCrossDissolve, animations: {
            self.window?.rootViewController = drawerController;
        }, completion: nil)
        window?.makeKeyAndVisible()
    }

    func setStatusBarBackgroundColor(color: UIColor) {
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        
        statusBar.backgroundColor = color
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "BTS")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

