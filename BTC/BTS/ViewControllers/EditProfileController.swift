//
//  EditProfileController.swift
//  BTS
//
//  Created by Pawan Ramteke on 29/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class EditProfileController: BaseViewController {
    
    var updateClosure : (()->())?

    @IBOutlet weak var baseView: UIView!
    
    var editType : String!
    var editTypeAccount = "Account"
    var editTypePersonalInfo = "Personal Info"
    
    @IBOutlet weak var btnChangeTopConstr: NSLayoutConstraint!
    @IBOutlet weak var viewAccountInfo: UIView!
    @IBOutlet weak var viewPersonalInfo: UIView!
    @IBOutlet weak var viewAddressInfo: UIView!
    
    
    @IBOutlet weak var txtFieldFirstName: MaterialTextField!
    @IBOutlet weak var txtFieldLastName: MaterialTextField!
    @IBOutlet weak var txtFieldEmail: MaterialTextField!

    @IBOutlet weak var txtFieldDob: MaterialTextField!
    @IBOutlet weak var txtFieldGender: MaterialTextField!
    @IBOutlet weak var txtFieldOccupation: MaterialTextField!
    @IBOutlet weak var txtFieldIncomeRange: MaterialTextField!
    
    @IBOutlet weak var txtFieldAddress: MaterialTextField!
    @IBOutlet weak var txtFieldLandmark: MaterialTextField!
    @IBOutlet weak var txtFieldPinCode: MaterialTextField!
    @IBOutlet weak var txtFieldCity: MaterialTextField!
    @IBOutlet weak var txtFieldState: MaterialTextField!
    
    @IBOutlet weak var btnChange: MaterialButton!
    var profileModel : MyProfileModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
       // baseView.removeConstraint(btnChangeTopConstr)
        viewAccountInfo.isHidden = true
        viewPersonalInfo.isHidden = true
        viewAddressInfo.isHidden = true
        
        txtFieldDob.setTextFieldTag(tag: .TAG_DATE_PICKER)
        
        txtFieldGender.setTextFieldTag(tag: .TAG_ACTION_SHEET)
        txtFieldGender.actionSheetData = ["Male","Female"]
        
        txtFieldOccupation.setTextFieldTag(tag: .TAG_DROP_DOWN)
        txtFieldOccupation.dropDownData = ["Service","Business","Student","House Wife","Retired"]
        
        txtFieldIncomeRange.setTextFieldTag(tag: .TAG_DROP_DOWN)
        txtFieldIncomeRange.dropDownData = ["0-5L","5-10L","10-15L","15-20L","20-25L","25L Above"]
        
        txtFieldState.setTextFieldTag(tag: .TAG_DROP_DOWN)
        txtFieldState.dropDownData = getStateData() as! [String]
        txtFieldState.onDataSelectionSuccess { (state) in
            self.txtFieldCity.dropDownData = self.getCity(fromState: state) as! [String]
            self.txtFieldCity.text = ""
        }
        
        txtFieldCity.setTextFieldTag(tag: .TAG_DROP_DOWN)
        
        if editType == editTypeAccount {
            self.lblNavTitle.text = "Account Details"
            btnChange.belowToView(view: viewAccountInfo, pixels: 20)
            viewAccountInfo.isHidden = false
        }
        else if editType == editTypePersonalInfo {
            self.lblNavTitle.text = "Personal Details"
             btnChange.belowToView(view: viewPersonalInfo, pixels: 20)
            viewPersonalInfo.isHidden = false
        }
        else{
            self.lblNavTitle.text = "Address Details"
             btnChange.belowToView(view: viewAddressInfo, pixels: 20)
            viewAddressInfo.isHidden = false
        }
        
        if profileModel != nil {
            setupProfileData()
        }
        
    }
    
    func updateProfileAPICall(param : [String : AnyObject])
    {
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: UPDATE_PROFILE_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            if self.updateClosure != nil {
                self.updateClosure!()
            }
            self.navigationController?.popViewController(animated: true)
            VIEWMANAGER.showToast(message: responseData as! String)
        }) { (strErr) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: strErr!)
        }
    }
    
    
    
    @IBAction func btnChangeClicked(_ sender: Any) {
        if editType == editTypeAccount
        {
            if validateForAccountInfo()
            {
                let param = [
                    "user_id":VIEWMANAGER.currentUser.bTCSubrUserID,
                    "fname": txtFieldFirstName.text!,
                    "lname": txtFieldLastName.text!,
                    "email": txtFieldEmail.text!,
                    "occupation": profileModel.occupation,
                    "incomeRange":profileModel.incomeRange,
                    "gender": profileModel.gender,
                    "dob": profileModel.dob.count > 0 ? formattedDate(strDate: profileModel.dob) : "",
                    "address_line": profileModel.addressLine,
                    "landmark":profileModel.landmark,
                    "pincode": profileModel.pincode,
                    "city": profileModel.city,
                    "state": profileModel.state,
                ]
                updateProfileAPICall(param: param as [String : AnyObject])
            }
            return
        }
        
        if editType == editTypePersonalInfo
        {
            if validateForPersonalInfo()
            {
                let param = [
                    "user_id":VIEWMANAGER.currentUser.bTCSubrUserID,
                    "fname": profileModel.fName,
                    "lname": profileModel.lName,
                    "email": profileModel.eMail,
                    "occupation": txtFieldOccupation.text,
                    "incomeRange":txtFieldIncomeRange.text,
                    "gender": txtFieldGender.text,
                    "dob": txtFieldDob.text,
                    "address_line": profileModel.addressLine,
                    "landmark":profileModel.landmark,
                    "pincode": profileModel.pincode,
                    "city": profileModel.city,
                    "state": profileModel.state,
                    ]
                updateProfileAPICall(param: param as [String : AnyObject])
            }
            return
        }
        
        if validateForAddressInfo(){
            
            let param = [
                        "user_id":VIEWMANAGER.currentUser.bTCSubrUserID,
                        "fname": profileModel.fName,
                        "lname": profileModel.lName,
                        "email": profileModel.eMail,
                        "occupation": profileModel.occupation,
                        "incomeRange":profileModel.incomeRange,
                        "gender": profileModel.gender,
                        "dob": profileModel.dob.count > 0 ? formattedDate(strDate: profileModel.dob) : "",
                        "address_line": txtFieldAddress.text!,
                        "landmark":txtFieldLandmark.text!,
                        "pincode": txtFieldPinCode.text!,
                        "city": txtFieldCity.text!,
                        "state": txtFieldState.text!
                        ]
            updateProfileAPICall(param: param as [String : AnyObject])
        }
    }
    
    func setupProfileData()
    {
        if editType == editTypeAccount {
            txtFieldFirstName.text = profileModel.fName
            txtFieldLastName.text = profileModel.lName
            txtFieldEmail.text = profileModel.eMail
        }
        else if editType == editTypePersonalInfo {
            txtFieldDob.text = profileModel.dob.count > 0 ? formattedDate(strDate: profileModel.dob) : ""
            txtFieldGender.text = profileModel.gender
            txtFieldOccupation.text = profileModel.occupation
            txtFieldIncomeRange.text = profileModel.incomeRange
        }
        else{
            txtFieldAddress.text = profileModel.addressLine
            txtFieldLandmark.text = profileModel.landmark
            txtFieldPinCode.text = profileModel.pincode
            txtFieldCity.text = profileModel.city
            txtFieldState.text = profileModel.state
            
            if profileModel.state.count > 0{
                self.txtFieldCity.dropDownData = self.getCity(fromState: profileModel.state) as! [String]
            }
        }
    }
    
    func validateForAccountInfo()->Bool
    {
        if txtFieldFirstName.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter first name")
            return false
        }
        if txtFieldLastName.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter last name")
            return false
        }
        if txtFieldEmail.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter email id")
            return false
        }
        if !VIEWMANAGER.isValidEmail(testStr: txtFieldEmail.text!) {
            VIEWMANAGER.showToast(message: "Please enter valid email id")
            return false

        }
        return true
    }
    
    func validateForPersonalInfo()->Bool
    {
        if txtFieldDob.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please select date of birth")
            return false
        }
        if txtFieldGender.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please select gender")
            return false
        }
        if txtFieldOccupation.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter occupation")
            return false
        }
        
        if txtFieldIncomeRange.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter income range")
            return false
        }
        return true
    }
    
    func validateForAddressInfo()->Bool
    {
        if txtFieldAddress.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter address")
            return false
        }
        if txtFieldLandmark.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter landmark")
            return false
        }
        if txtFieldPinCode.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter pin code")
            return false
        }
        if txtFieldState.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter state")
            return false
        }
        if txtFieldCity.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter city")
            return false
        }
        return true
    }
    
    func onUpdateProfile(closure : @escaping () -> Void)
    {
        updateClosure = closure
    }
    
    func getStateData() -> NSArray
    {
        if let path = Bundle.main.path(forResource: "state-city", ofType: "geojson") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? NSDictionary {
                    let arrState = jsonResult.allKeys
                    let sortedResults: NSArray = arrState.sorted { ($0 as! String).localizedCaseInsensitiveCompare($1 as! String) == ComparisonResult.orderedAscending } as NSArray

                    return sortedResults as NSArray
                }
            } catch {
                // handle error
            }
        }
        return []
    }
    
    func getCity(fromState:String) -> NSArray
    {
        if let path = Bundle.main.path(forResource: "state-city", ofType: "geojson") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? NSDictionary {
                    let arrCity = jsonResult.object(forKey: fromState) as! NSArray
                    let sortedResults: NSArray = arrCity.sorted { ($0 as! String).localizedCaseInsensitiveCompare($1 as! String) == ComparisonResult.orderedAscending } as NSArray
                    
                    return sortedResults as NSArray
                }
            } catch {
                // handle error
            }
        }
        return []
    }
    
    func formattedDate(strDate:String) -> String
    {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"
        let date = dateFormat.date(from: strDate)

        if let aDate = date {
            var dateString: String? = nil
            dateFormat.dateFormat = "yyyy-MM-dd"
            dateString = dateFormat.string(from: aDate)
            return dateString!
        }
        
        return "-"
    }
}
