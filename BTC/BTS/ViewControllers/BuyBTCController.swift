//
//  BuyBTCController.swift
//  BTS
//
//  Created by Pawan Ramteke on 18/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import CoreLocation
import MapKit
class BuyBTCController: BaseViewController,UITextFieldDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,GMSAutocompleteViewControllerDelegate {
    
    var isBuy : Bool! = false
    @IBOutlet weak var txtFieldNoOfBTC: MaterialTextField!
    @IBOutlet weak var mapContainer: UIView!
    var locationManager: CLLocationManager!
    var mapView : GMSMapView!
    var lblAddress : UILabel!
    var currentLocation : CLLocationCoordinate2D!
    @IBOutlet weak var lblInrValue: UILabel!
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblNavTitle.text = isBuy ?  "I want to buy BTC" : "I want to sell BTC"
        
        txtFieldNoOfBTC.placeholder = isBuy ? "Enter number of BTC you want to buy" : "Enter number of BTC you want to sell"
        txtFieldNoOfBTC.delegate = self
        initializeLocationManager()
        setupMap()
    }
    
    //MARK: - Location manager
    func initializeLocationManager()
    {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        else{
            VIEWMANAGER.showToast(message: "Location is not enabled")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        updateMyLocation(location: manager.location!)
    }
    
    func updateMyLocation(location:CLLocation,zoom : Float = 16.0)
    {
        mapView.clear()
        let locValue:CLLocationCoordinate2D = location.coordinate
        self.currentLocation = locValue

        let camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: zoom)
        mapView.animate(to: camera)
        
        getLocationAddress(location:location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    func getLocationAddress(location:CLLocation)
    {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location) { (placeMarks, error) in
            if error == nil && placeMarks!.count > 0
            {
                let placeMark  = placeMarks?.last as! CLPlacemark
                let marker = GMSMarker(position: location.coordinate)
                marker.title = self.getAddress(placemark: placeMark)
                marker.map = self.mapView
                
                self.lblAddress.text = marker.title
            }
        }
    }
    
    
    //MARK: - Setup Map
    func setupMap()
    {
        mapView = GMSMapView(frame: CGRect.zero)
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        mapView.settings.myLocationButton = true
        mapContainer.addSubview(mapView)
        mapView.enableAutoLayout()
        mapView.leadingMargin(pixels: 0)
        mapView.trailingMargin(pixels: 0)
        mapView.topMargin(pixels: 0)
        mapView.bottomMargin(pixels: 0)
        
        let viewBase = UIView()
        viewBase.backgroundColor = UIColor.white
        mapContainer.addSubview(viewBase)
        viewBase.enableAutoLayout()
        viewBase.leadingMargin(pixels: 10)
        viewBase.trailingMargin(pixels: 10)
        viewBase.topMargin(pixels: 10)
        
        let btnSearch = UIButton()
        btnSearch.setImage(UIImage(named:"ic_search"), for: .normal)
        btnSearch.setTitle("Search", for: .normal)
        viewBase.addSubview(btnSearch)
        btnSearch.titleEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        btnSearch.titleLabel?.font = UIFont.appRegularFont(size: 18)
        btnSearch.setTitleColor(UIColor.darkGray, for: .normal)
        btnSearch.contentHorizontalAlignment = .left
        btnSearch.addTarget(self, action: #selector(btnSearchLocationClicked), for: .touchUpInside)
        btnSearch.enableAutoLayout()
        btnSearch.leadingMargin(pixels: 10)
        btnSearch.topMargin(pixels: 0)
        btnSearch.trailingMargin(pixels: 10)
        btnSearch.fixedHeight(pixels: 40)
        
        lblAddress = UILabel()
        lblAddress.font = UIFont.appRegularFont(size: 16)
        lblAddress.numberOfLines = 0
        viewBase.addSubview(lblAddress)
        lblAddress.enableAutoLayout()
        lblAddress.leadingMargin(pixels: 10)
        lblAddress.belowToView(view: btnSearch, pixels:0)
        lblAddress.trailingMargin(pixels: 10)
        lblAddress.bottomMargin(pixels: -10)
    }
    
    //MARK: - GMS Map view delegate
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        updateMyLocation(location: mapView.myLocation!)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        updateMyLocation(location: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude),zoom: mapView.camera.zoom)
    }
    
    //MARK: - API Call

    func postRequestAPICall()
    {
        let param : [String : AnyObject] = [
            "user_id":VIEWMANAGER.currentUser.bTCSubrUserID as AnyObject,
            "address":lblAddress.text as AnyObject,
            "lat":String(currentLocation.latitude) as AnyObject,
            "lng": String(currentLocation.longitude) as AnyObject,
            "btcNo":txtFieldNoOfBTC.text as AnyObject
            ]
        
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: isBuy ? BUY_BTC_API : SELL_BTC_API, params: param, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: response as! String)
            self.pushToMyWalletController()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }

    }
    
    //MARK: - Button Clicked
    @IBAction func btnPostRequestClicked(_ sender: Any) {
        if txtFieldNoOfBTC.text?.count == 0 {
            VIEWMANAGER.showToast(message: "Please \(String(describing: txtFieldNoOfBTC.placeholder!))")
            return
        }
        
        if !isBuy && Float(VIEWMANAGER.rateModel.btcAccBalance!) == 0   {
            CustomAlertView.showAlert(withTitle: "Insufficient Balance !!!!", messsage: "Your account is not sufficient to sell the number of BTC you have entered.", completion: nil)
            return
        }
        
        if !isBuy && Float(VIEWMANAGER.rateModel.btcAccBalance!)! < Float(txtFieldNoOfBTC.text!)!   {
            CustomAlertView.showAlert(withTitle: "Insufficient Balance !!!!", messsage: "Your account is not sufficient to sell the number of BTC you have entered.", completion: nil)
            return
        }
        if currentLocation == nil || !CLLocationManager.locationServicesEnabled(){
            CustomAlertView.showPermissionEnableAlert(title: "Location Disabled", message: "To send location, go to your Settings\nApp > Privacy > Location Services", completion: {
                let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/")
                if UIApplication.shared.canOpenURL(url!) {
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                }
            })
            return
        }
        postRequestAPICall()
    }
    
    @objc func btnSearchLocationClicked()
    {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        self.present(acController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        mapView.clear()
        currentLocation = place.coordinate
        lblAddress.text = place.formattedAddress
        let marker = GMSMarker(position: place.coordinate)
        marker.title = place.formattedAddress
        marker.map = self.mapView

        self.dismiss(animated: true) {
            let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 16)
            self.mapView.animate(to: camera)
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Helper
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,let textRange = Range(range, in: text)
        {
            let updatedText = text.replacingCharacters(in: textRange,with: string)
            let noOfBTC =  updatedText == "" ? 0 : NumberFormatter().number(from: updatedText)!.floatValue
            let currentRate = NumberFormatter().number(from: VIEWMANAGER.rateModel.btcRate!)!.floatValue
            let rate = noOfBTC * currentRate
            lblInrValue.text = NSString(format: "%.1f INR", rate) as String
        }
        return true
    }

    
    func getAddress(placemark:CLPlacemark) -> String
    {
        var strAdd : String = ""
        if (placemark.subThoroughfare != nil && placemark.subThoroughfare?.count != 0){
            strAdd = placemark.subThoroughfare!
        }
        
        if (placemark.thoroughfare != nil && placemark.thoroughfare?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.thoroughfare!)
            }
            else{
                strAdd = placemark.thoroughfare!
            }
        }
        
        if (placemark.subLocality != nil && placemark.subLocality?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.subLocality!)
            }
            else{
                strAdd = placemark.subLocality!;
            }
        }
        
        if (placemark.locality != nil && placemark.locality?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.locality!)
            }
            else{
                strAdd = placemark.locality!;
            }
        }
        
        if (placemark.administrativeArea != nil && placemark.administrativeArea?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.administrativeArea!)
            }
            else{
                strAdd = placemark.administrativeArea!;
            }
        }
        
        if (placemark.country != nil && placemark.country?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.country!)
            }
            else{
                strAdd = placemark.country!;
            }
        }
        
        if (placemark.postalCode != nil && placemark.postalCode?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.postalCode!)
            }
            else{
                strAdd = placemark.postalCode!
            }
        }
        
        return strAdd
    }
    
    func pushToMyWalletController()
    {
        let myWalletVC = self.storyboard?.instantiateViewController(withIdentifier:"MyBTCWalletController") as! MyBTCWalletController
        myWalletVC.fromBuySellBTC = true
        self.navigationController?.pushViewController(myWalletVC, animated: true)
    }
}
