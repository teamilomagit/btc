//
//  LoginViewController.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    
    @IBOutlet weak var header: UIView!

    @IBOutlet weak var lblTItle: UILabel!
    @IBOutlet weak var btnDontHaveAcc: UIButton!
    @IBOutlet weak var txtFieldMobNo: MaterialTextField!
    @IBOutlet weak var txtFieldPassword: MaterialTextField!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    var gradient : CAGradientLayer!
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTItle.textColor = .white
        lblTItle.font = UIFont.appBoldFont(size: 16)
        btnForgotPassword.titleLabel?.font = UIFont.appRegularFont(size: 16)
        btnDontHaveAcc.setTitleColor(.appThemeColor, for: .normal)
        btnDontHaveAcc.titleLabel?.font = UIFont.appRegularFont(size: 16)
        txtFieldMobNo.setTextFieldTag(tag: .TAG_MOBILE)
        txtFieldPassword.setTextFieldTag(tag: .TAG_PIN_NUMBER)
        setupHeader()
    }
    
    func setupHeader()
    {
        gradient = CAGradientLayer()
        gradient.frame = header.bounds
        gradient.colors = [UIColor.white.withAlphaComponent(1.0).cgColor,UIColor.appThemeColor.cgColor]
        header.layer.insertSublayer(gradient, at: 0)
    }
    
    // MARK: - API Call
    func loginAPICall()
    {
        let param : [String : AnyObject] = [
                                        "txtusername":txtFieldMobNo.text as AnyObject,
                                        "txtpassword":txtFieldPassword.text as AnyObject
                                        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: LOGIN_API, params: param, completion: { (responseData) in
            VIEWMANAGER.currentUser = responseData as! LoginModel
            self.pushToDashboard()
            VIEWMANAGER.hideActivityIndicator()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
    }

    // MARK: - Button Click Events
    @IBAction func btnLoginClicked(_ sender: Any) {
        self.view.endEditing(true)
        if validate() {
            loginAPICall()
        }
    }
    
    @IBAction func btnDontHaveAccClicked(_ sender: Any) {
        pushToSignUpController()
    }
    
    @IBAction func btnForgotPasswordClicked(_ sender: Any) {
        let forgotPasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordController")
        self.navigationController?.pushViewController(forgotPasswordVC!, animated: true)
    }
    
    // MARK: - Helper
    
    func validate() -> Bool
    {
        if txtFieldMobNo.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter mobile number or email id")
            return false
        }
        if (txtFieldPassword.text?.trimString().count)! < 6 {
            VIEWMANAGER.showToast(message: "Please enter 6 digit pin number")
            return false
        }
        return true
    }
    
    func pushToSignUpController()
    {
//       let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController")
//        self.navigationController?.pushViewController(signupVC!, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    func pushToDashboard()
    {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.setDashboardAsRoot()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradient.frame = header.bounds
    }
}
