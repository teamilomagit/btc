//
//  ForgotPasswordController.swift
//  BTS
//
//  Created by Pawan Ramteke on 25/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class ForgotPasswordController: BaseViewController {

    @IBOutlet weak var header: UIView!
    @IBOutlet weak var lblTItle: UILabel!
    
    @IBOutlet weak var lblDescr: UILabel!
    
    @IBOutlet weak var txtFieldMobileNo: MaterialTextField!
    
    @IBOutlet weak var txtFieldOTP: MaterialTextField!
    var gradient : CAGradientLayer!

    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTItle.textColor = .white
        lblTItle.font = UIFont.appBoldFont(size: 16)
        lblDescr.font = UIFont.appRegularFont(size: 16)
        txtFieldMobileNo.setTextFieldTag(tag: .TAG_MOBILE)
        
        setupHeader()
    }
    
    func setupHeader()
    {
        gradient = CAGradientLayer()
        gradient.frame = header.bounds
        gradient.colors = [UIColor.white.withAlphaComponent(1.0).cgColor,UIColor.appThemeColor.cgColor]
        header.layer.insertSublayer(gradient, at: 0)
    }

     // MARK: - API Call
    func sendOTPApiCall()
    {
        let param : [String : AnyObject] = [
            "phone" : txtFieldMobileNo.text as AnyObject
                    ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: SEND_OTP_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: responseData as! String)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
    }
    
    func verifyOTPApiCall()
    {
        let param : [String : AnyObject] = [
            "phone" : txtFieldMobileNo.text! as AnyObject,
            "otp_code" : txtFieldOTP.text! as AnyObject
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: VERIFY_OTP_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.pushToResetPasswordController()
            self.txtFieldMobileNo.text = ""
            self.txtFieldOTP.text = ""
            VIEWMANAGER.showToast(message: responseData as! String)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
    }
    
    // MARK: - Button Clicked
    @IBAction func btnSendOTPClicked(_ sender: Any) {
        self.view.endEditing(true)
        if txtFieldMobileNo.text!.trimString().count < 10 {
            VIEWMANAGER.showToast(message: "Please enter valid Mobile Number")
            return
        }
        sendOTPApiCall()
    }
    
    @IBAction func btnVerifyClicked(_ sender: Any) {
        self.view.endEditing(true)

        if txtFieldMobileNo.text!.trimString().count < 10 {
            VIEWMANAGER.showToast(message: "Please enter valid Mobile Number")
            return
        }
        if txtFieldOTP.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter OTP")
            return
        }
        verifyOTPApiCall()
    }
    
    
    // MARK: - Helper
    func pushToResetPasswordController()
    {
        let resetPassVC = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordController") as! ResetPasswordController
        resetPassVC.mobileNo = txtFieldMobileNo.text!
        self.navigationController?.pushViewController(resetPassVC, animated: true)

    }
}
