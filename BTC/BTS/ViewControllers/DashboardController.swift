//
//  DashboardController.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import KYDrawerController
class DashboardController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    
    let page_size : Int = 15
    var start_index : Int = 0
    var end_index : Int = 15
    var loadStarted : Bool = false

    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    var historyArray = NSMutableArray()
    
    //MARK: -
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
       setupForNavigation()
        tblView.separatorStyle = .none
        tblView.register( UINib(nibName: "DashboardTableCell", bundle: nil), forCellReuseIdentifier: "CellId")
        getBTCHistoryAPICall(startIdx: start_index, endIndex: end_index)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DashboardController.drawerSelectionNotification(notification:)), name: NSNotification.Name(rawValue: DRAWER_SELECTION_NOTIFICATION), object: nil)
    }
    
    
    func setupForNavigation()
    {
        self.headerView.btnBack.setImage(UIImage(named:"ic_drawer"), for: .normal)
        self.headerView.onBackClicked {
            self.btnNavigationClicked()
        }
        self.headerView.lblHeaderTitle.text = "Welcome to BTC World!"
        //  self.headerView.lblHeaderTitle.textAlignment = .center
        
    }
    
    //MARK: - API Call

    func getBTCHistoryAPICall(startIdx:Int,endIndex:Int,showLoader:Bool = true)
    {
        let param : [String : AnyObject] = [
            "user_id":VIEWMANAGER.currentUser.bTCSubrUserID as AnyObject,
            "start_index":startIdx as AnyObject,
            "end_index":endIndex as AnyObject
        ]
        if showLoader {
            VIEWMANAGER.showActivityIndicator()
        }
        RemoteAPI().callPOSTApi(apiName: BTC_HISTORY_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            
            let arrData = responseData as! NSArray
            
            self.historyArray.addObjects(from: responseData as! [Any])
            self.tblView.reloadData()
            
            if arrData.count == self.page_size
            {
                self.start_index = self.historyArray.count
                self.end_index = self.end_index + self.page_size
                self.loadStarted = true
                self.loader.startAnimating()
            }
            else{
                self.loadStarted = false
                self.loader.stopAnimating()
            }
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
    }
    
    //MARK: - Table datadource and delegatess
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellId") as! DashboardTableCell
        cell.backgroundColor = indexPath.row % 2 == 0 ? .white : UIColor(red: 0.96, green: 0.95, blue: 0.95, alpha: 1.0)
    
        let model = historyArray[indexPath.row] as! BTCHistoryModel
        cell.lblDate.text = model.date
        cell.lblRate.text = "\(model.btcRate!) INR"
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 35))
        header.backgroundColor = UIColor.appThemeColor
        
        let lblDate = UILabel(frame: CGRect(x: 0, y: 0, width: header.frame.size.width/2, height: header.frame.size.height))
        lblDate.font = UIFont.appRegularFont(size: 18)
        lblDate.textColor = .white
        lblDate.text = "Date"
        lblDate.textAlignment = .center
        header.addSubview(lblDate)
        
        let lblRate = UILabel(frame: CGRect(x: lblDate.frame.maxX, y: 0, width: lblDate.frame.size.width, height: header.frame.size.height))
        lblRate.font = UIFont.appRegularFont(size: 18)
        lblRate.textColor = .white
        lblRate.text = "BTC Rate"
        lblRate.textAlignment = .center
        header.addSubview(lblRate)
        
        let separator = UIView(frame: CGRect(x: header.frame.midX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator.backgroundColor = .white
        header.addSubview(separator)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    //MARK: - Button Clicked Events
    func btnNavigationClicked() {
        var parentVC = self.parent
        while parentVC != nil {
            if let drawerVC = parentVC as? KYDrawerController
            {
                drawerVC.setDrawerState(.opened, animated: true)
            }
            parentVC = parentVC?.parent
        }
    }
    
    @IBAction func btnBuyBTCClicked(_ sender: Any) {
        let buyBTCVC = self.storyboard?.instantiateViewController(withIdentifier: "BuyBTCController") as! BuyBTCController
        buyBTCVC.isBuy = true
        self.navigationController?.pushViewController(buyBTCVC, animated: true)
    }
    
    @IBAction func btnSaleBTC(_ sender: Any) {
        let buyBTCVC = self.storyboard?.instantiateViewController(withIdentifier: "BuyBTCController") as! BuyBTCController
        self.navigationController?.pushViewController(buyBTCVC, animated: true)

    }
    
    @IBAction func btnTransferBTCClicked(_ sender: Any) {
        let transferBTCVC = self.storyboard?.instantiateViewController(withIdentifier: "TransferBTCController") as! TransferBTCController
        self.navigationController?.pushViewController(transferBTCVC, animated: true)

    }
    
    @IBAction func btnBuyerListClicked(_ sender: Any) {
        let buyerListVC = self.storyboard?.instantiateViewController(withIdentifier: "NearestBuyerSalerController") as! NearestBuyerSalerController
        buyerListVC.isBuyerList = true
        self.navigationController?.pushViewController(buyerListVC, animated: true)
    }
    
    @IBAction func btnSalerListClicked(_ sender: Any) {
        let salerListVC = self.storyboard?.instantiateViewController(withIdentifier: "NearestBuyerSalerController") as! NearestBuyerSalerController
        salerListVC.isBuyerList = false
        self.navigationController?.pushViewController(salerListVC, animated: true)
    }
    
    @IBAction func btnMyACStmtClicked(_ sender: Any) {
        let myAccStmtVC = self.storyboard?.instantiateViewController(withIdentifier: "YourAccStatementController") as! YourAccStatementController
        self.navigationController?.pushViewController(myAccStmtVC, animated: true)
    }
    
    //MARK: - Helper
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tblView.contentOffset.y >= (tblView.contentSize.height - tblView.frame.size.height) {
            if loadStarted {
                getBTCHistoryAPICall(startIdx: start_index, endIndex: end_index,showLoader: false)
                loadStarted = false
            }
        }
    }
    
    
    @objc func drawerSelectionNotification(notification:NSNotification)
    {
        let index : NSInteger = notification.object as! NSInteger
        switch index {
        case 0:
            pushToMyProfileController()
            break
        
        case 1:
            pushToWebViewController(url: ABOUT_BTC_URL, title: "About Us")
            break
            
        case 2:
            pushToWebViewController(url: CUSTOMER_SUPPORT_URL, title: "Customer Support")
            break
            
        case 3:
            referAFriend()
            break
            
        case 4:
            pushToWebViewController(url: PRIVACY_POLICY_URL, title: "Privacy Policy")
            break
            
        case 5:
            pushToWebViewController(url: TERMS_AND_CONDITION_URL, title: "Terms and Condititon")
            break
        case 6:
            pushToChangePasswordController()
            break
        case 7:
            showLogoutView()
            break
        default:
            break
        }
    }
    
    func pushToMyProfileController()
    {
        let profileVC : MyProfileController = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileController") as! MyProfileController
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func pushToWebViewController(url : String, title : String)
    {
        let webVC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        webVC.urlToLoad = url
        webVC.navTitle = title
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    
    func pushToChangePasswordController()
    {
        let changePassVCs = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordController") as! ChangePasswordController
        self.navigationController?.pushViewController(changePassVCs, animated: true)
    }
    
    func showLogoutView()
    {
        let logoutView = LogoutView(frame: self.view.bounds)
        self.view.addSubview(logoutView)
    }
    
    func referAFriend()
    {
        let myShare = "Hey... I am using BTC India Munafa Coin App for generating 1% profit per day. Download the App by click on below link and Grow your money guarunted. Click here \(APP_STORE_LINK)"
        
        let shareVC: UIActivityViewController = UIActivityViewController(activityItems: [myShare], applicationActivities: nil)
        shareVC.excludedActivityTypes = [.print, .postToWeibo,.addToReadingList,.postToVimeo]
        
        self.present(shareVC, animated: true, completion: nil)
    }
    
}
