//
//  DrawerViewController.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import KYDrawerController
import SDWebImage
class DrawerViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    let arrDrawerList = ["My Profile","About BTC","Customer Support","Reffer to friend","Privacy Policy","Terms and conditions","Change Password","Logout"]
    let arrmages = ["ic_drawer_profile","ic_drawer_about","ic_drawer_cust_support","ic_drawer_refer_friend","ic_privacy_policy","ic_drawer_termsncond","ic_drawer_change_pass","ic_logout",]
    var imgViewDP : UIImageView!
    var lblUserName : UILabel!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imgViewDP.sd_setImage(with: URL(string:VIEWMANAGER.currentUser.profilePic), placeholderImage: UIImage(named:"ic_profile"), options: SDWebImageOptions.allowInvalidSSLCertificates, completed: nil)
        lblUserName.text = "\(VIEWMANAGER.currentUser.fName!) \(VIEWMANAGER.currentUser.lName!)"

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.register( UINib(nibName: "DrawerTableCell", bundle: nil), forCellReuseIdentifier: "CellId")
        tblView.separatorStyle = .none
        tblView.tableHeaderView = tableHeaderView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onProfilePicChanged(_:)), name: NSNotification.Name(rawValue: "ProfileUpdateNotificcation"), object: nil)
    }

    func tableHeaderView()->UIView
    {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width * 0.8, height: 220))
        
        let baseView = MaterialView(frame: CGRect(x: 10, y: 10, width: header.bounds.size.width - 20, height: header.frame.size.height - 20))
        header.addSubview(baseView)
        
        imgViewDP = UIImageView()
        imgViewDP.sd_setImage(with: URL(string:VIEWMANAGER.currentUser.profilePic), placeholderImage: UIImage(named:"ic_profile"), options: SDWebImageOptions.allowInvalidSSLCertificates, completed: nil)
        imgViewDP.layer.cornerRadius = 30
        imgViewDP.layer.masksToBounds = true
        imgViewDP.layer.borderWidth = 2.0
        imgViewDP.layer.borderColor = UIColor.appThemeColor.cgColor
        imgViewDP.contentMode = .scaleAspectFill
        baseView.addSubview(imgViewDP)
        imgViewDP.enableAutoLayout()
        imgViewDP.leadingMargin(pixels: 20)
        imgViewDP.topMargin(pixels: 10)
        imgViewDP.fixedWidth(pixels: 60)
        imgViewDP.fixedHeight(pixels: 60)
        
        let btnShowDP = UIButton()
        header.addSubview(btnShowDP)
        btnShowDP.addTarget(self, action: #selector(DrawerViewController.btnShowImageClicked), for: .touchUpInside)
        btnShowDP.enableAutoLayout()
        btnShowDP.leadingMargin(pixels: 20)
        btnShowDP.topMargin(pixels: 10)
        btnShowDP.fixedWidth(pixels: 60)
        btnShowDP.fixedHeight(pixels: 60)

        
        lblUserName = UILabel()
        lblUserName.font = UIFont.appBoldFont(size: 16)
        lblUserName.text = "\(VIEWMANAGER.currentUser.fName!) \(VIEWMANAGER.currentUser.lName!)"
        baseView.addSubview(lblUserName)
        lblUserName.enableAutoLayout()
        lblUserName.addToRightToView(view: imgViewDP, pixels: 10)
        lblUserName.trailingMargin(pixels: 10)
        lblUserName.topMargin(pixels: 20)
        
        let lblMobileNo = UILabel()
        lblMobileNo.font = UIFont.appRegularFont(size: 14)
        lblMobileNo.text = "\(VIEWMANAGER.currentUser.mobile!)"
        baseView.addSubview(lblMobileNo)
        lblMobileNo.enableAutoLayout()
        lblMobileNo.addToRightToView(view: imgViewDP, pixels: 10)
        lblMobileNo.trailingMargin(pixels: 10)
        lblMobileNo.belowToView(view: lblUserName, pixels: 5)
        
        let imgViewQRCode = UIImageView()
        imgViewQRCode.sd_setImage(with: URL(string:VIEWMANAGER.currentUser.qrCode), placeholderImage: UIImage(named:""), options: SDWebImageOptions.allowInvalidSSLCertificates, completed: nil)
        
        baseView.addSubview(imgViewQRCode)
        imgViewQRCode.enableAutoLayout()
        imgViewQRCode.centerX()
        imgViewQRCode.belowToView(view: imgViewDP, pixels:10)
        imgViewQRCode.fixedWidth(pixels: 100)
        imgViewQRCode.fixedHeight(pixels: 100)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDrawerList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellId") as! DrawerTableCell
        cell.lblTitle.text = arrDrawerList[indexPath.row]
        cell.imgView.image = UIImage(named: arrmages[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = self.parent as! KYDrawerController
        controller.setDrawerState(.closed, animated: true)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: DRAWER_SELECTION_NOTIFICATION), object: indexPath.row)
    }
    
    @objc func btnShowImageClicked()
    {
        let imgViewerVC = ImageViewerController()
        imgViewerVC.imgURL = VIEWMANAGER.currentUser.profilePic
        imgViewerVC.modalPresentationStyle = .custom
        imgViewerVC.modalTransitionStyle = .crossDissolve
        self.present(imgViewerVC, animated: true, completion: nil)
    }
    
    @objc func onProfilePicChanged(_ notification:Notification){
        
        SDImageCache.shared().removeImage(forKey: VIEWMANAGER.currentUser.profilePic) {
            self.imgViewDP.image = UIImage(named:"ic_profile")
            self.imgViewDP.sd_setImage(with: URL(string:VIEWMANAGER.currentUser.profilePic), placeholderImage: UIImage(named:"ic_profile"), options: SDWebImageOptions.allowInvalidSSLCertificates, completed: nil)
        }
    }
}
