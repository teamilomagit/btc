//
//  StatementDetailsController.swift
//  BTS
//
//  Created by Pawan Ramteke on 31/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import SDWebImage

class StatementDetailsController: BaseViewController {

    var transID : String!

    @IBOutlet weak var imgViewDP: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblReqDate: UILabel!
    @IBOutlet weak var lblReqType: UILabel!
    @IBOutlet weak var lblReqBTC: UILabel!
    
    var detailsModel : AccStmtDetailsModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.headerView.lblHeaderTitle.text = "AC Statement Details"
        
        imgViewDP.layer.borderWidth = 2.0
        imgViewDP.layer.borderColor = UIColor.appThemeColor.cgColor
        
        getAccountStmtDetailsAPICall()

        // Do any additional setup after loading the view.
    }

    func getAccountStmtDetailsAPICall()
    {
        let param = [
            "user_id" : VIEWMANAGER.currentUser.bTCSubrUserID,
            "transID" : transID
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_ACCOUNT_STATEMENT_DETAILS_API, params: param as [String : AnyObject], completion: { (respose) in
            VIEWMANAGER.hideActivityIndicator()
            self.detailsModel = respose as! AccStmtDetailsModel
            self.setupUserData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
    }
    
    func setupUserData()
    {
        lblName.text = "\(detailsModel.fName!) \(detailsModel.lName!)"
        lblMobileNo.text = detailsModel.mobile
        lblReqDate.text = detailsModel.tDate
        lblReqType.text = detailsModel.tType
        lblReqBTC.text = "\(detailsModel.tBTC!) BTC"
      //  lblReqBTCBalance.text = "\(detailsModel.tBalance!) BTC"
      //  lblReqLocation.text = detailsModel.address == "" ? "-" : detailsModel.address
        
        imgViewDP.sd_setImage(with: URL(string:detailsModel.profilePic), placeholderImage: UIImage(named:"ic_profile"), options: SDWebImageOptions.allowInvalidSSLCertificates, completed: nil)
        
    }
    
    @IBAction func btnShowProfilePicClicked(_ sender: Any) {
        let imgViewerVC = ImageViewerController()
        imgViewerVC.imgURL = detailsModel.profilePic
        imgViewerVC.modalPresentationStyle = .custom
        imgViewerVC.modalTransitionStyle = .crossDissolve
        self.present(imgViewerVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnCallNowClicked(_ sender: Any) {
        let callURL = URL(string: "tel://\(detailsModel.mobile!)")
        
        if callURL != nil && UIApplication.shared.canOpenURL(callURL!) {
            
            UIApplication.shared.open(callURL!, options: [:], completionHandler: nil)
        }
    }
}

