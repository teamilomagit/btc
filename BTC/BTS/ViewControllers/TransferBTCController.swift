//
//  TransferBTCController.swift
//  BTS
//
//  Created by Pawan Ramteke on 22/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
class TransferBTCController: BaseViewController,UITextFieldDelegate {
    
    var txtFieldMobileNo : MaterialTextField!
    var viewQRReader : QRCodeReaderView!
    var btnProceed : MaterialButton!
    //MARK: -
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if viewQRReader != nil {
            self.viewQRReader.startReading()
        }
        txtFieldMobileNo.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtFieldMobileNo = MaterialTextField(frame: CGRect(x: 10, y:self.viewNavigation.frame.maxY+(SCREEN_HEIGHT >= 812 ? 40 : 20) , width: self.view.frame.size.width - 20, height: 50))
        txtFieldMobileNo.font = UIFont.appBoldFont(size: 22)
        txtFieldMobileNo.placeholder = "Mobile Number"
        txtFieldMobileNo.delegate = self
        txtFieldMobileNo.setTextFieldTag(tag: .TAG_MOBILE)
        self.view.addSubview(txtFieldMobileNo)
        
        
        btnProceed = MaterialButton(frame: CGRect(x: 0, y: self.view.bounds.maxY - 50 - (SCREEN_HEIGHT >= 812 ? 50 : 0), width: self.view.frame.size.width, height: 50))
        btnProceed.setTitle("Proceed", for: .normal)
        btnProceed.addTarget(self, action: #selector(TransferBTCController.btnProceedClicked), for: .touchUpInside)
        self.view.addSubview(btnProceed)
        
        viewQRReader = QRCodeReaderView(frame: CGRect(x: 0, y: txtFieldMobileNo.frame.maxY + 20, width: SCREEN_WIDTH, height: SCREEN_HEIGHT - txtFieldMobileNo.frame.maxY - 20 - btnProceed.frame.size.height - (SCREEN_HEIGHT >= 812 ? 50 : 0)))
        
        self.view.addSubview(viewQRReader)
        viewQRReader.onQRReadSuccess { (QRString) in
            self.checkBTCUserAPICall(walletID: QRString)
        }
    }
    
    
    //MARK: - API Call
    
    func checkBTCUserAPICall(walletID:String)
    {
        let param = [
            "user_id" : VIEWMANAGER.currentUser.bTCSubrUserID,
            "walletID": walletID
                    ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: CHECK_BTC_USER_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.pushToBTCTransferDetailsController(userModel: responseData as! UserModel)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
    }
    
    //MARK: - Button clicked events
    
    @objc func btnProceedClicked() {
        self.view.endEditing(true)
        if txtFieldMobileNo.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter Mobile Number or Wallet ID")
            return
        }
        checkBTCUserAPICall(walletID: txtFieldMobileNo.text!)
    }
    
    //MARK: - Helper
    
    func setupUserData()
    {
       // txtFieldFullName.text = userModel.sName
       // txtFieldCity.text = userModel.sCity
    }
    
    func pushToBTCTransferDetailsController(userModel : UserModel)
    {
        let transferVTCVCs = self.storyboard?.instantiateViewController(withIdentifier: "TransferBTCDetailsController") as! TransferBTCDetailsController
        transferVTCVCs.userModel = userModel
        self.navigationController?.pushViewController(transferVTCVCs, animated: true)

    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        btnProceed.frame.size.height = 50
//        viewQRReader.frame.size.height = viewQRReader.frame.size.height - 100
    }
}
