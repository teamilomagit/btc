//
//  MyBTCWalletController.swift
//  BTS
//
//  Created by Pawan Ramteke on 26/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import SDWebImage
class MyBTCWalletController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var imgViewDp: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var tblHtConstr: NSLayoutConstraint!
    
    let page_size : Int = 15
    var start_index : Int = 0
    var end_index : Int = 15
    var loadStarted : Bool = false

    var fromBuySellBTC : Bool = false
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var arrRequests = NSMutableArray()
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgViewDp.layer.borderWidth = 2.0
        imgViewDp.layer.borderColor = UIColor.appThemeColor.cgColor
        imgViewDp.sd_setImage(with: URL(string:VIEWMANAGER.currentUser.profilePic), placeholderImage: UIImage(named:"ic_profile"), options: SDWebImageOptions.allowInvalidSSLCertificates, completed: nil)
        
        imgQRCode.sd_setImage(with: URL(string:VIEWMANAGER.currentUser.qrCode), placeholderImage: UIImage(named:""), options: SDWebImageOptions.allowInvalidSSLCertificates, completed: nil)

        lblUserName.text = "\(VIEWMANAGER.currentUser.fName!) \(VIEWMANAGER.currentUser.lName!)"
        lblMobileNo.text = "\(VIEWMANAGER.currentUser.mobile!)"
        
        tblView.separatorStyle = .none
        tblView.register( UINib(nibName: "MyWalletCell", bundle: nil), forCellReuseIdentifier: "MyWalletCell")
        tblView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        getMyPostRequestAPICall(startIdx: start_index, endIndex: end_index)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblHtConstr.constant = tblView.contentSize.height
    }
    
    //MARK: - API Call
    func getMyPostRequestAPICall(startIdx:Int,endIndex:Int,showLoader:Bool = true)
    {
        let param = [
            "user_id" : VIEWMANAGER.currentUser.bTCSubrUserID,
            "start_index":startIdx,
            "end_index":end_index
        ] as [String : AnyObject]
        
        if showLoader {
            VIEWMANAGER.showActivityIndicator()
        }
        RemoteAPI().callPOSTApi(apiName: MY_BTC_REQUEST_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            let arrData = responseData as! NSArray
            
            self.arrRequests.addObjects(from: responseData as! [Any])
            self.tblView.reloadData()
            
            if arrData.count == self.page_size
            {
                self.start_index = self.arrRequests.count
                self.end_index = self.end_index + self.page_size
                self.loadStarted = true
                self.loader.startAnimating()
            }
            else{
                self.loadStarted = false
                self.loader.stopAnimating()
            }
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
    }
    
    func deleteMyRequestAPICall(indexPath:IndexPath)
    {
        let model = self.arrRequests[indexPath.row] as! MyBTCRequestModel

        let param = [
            "user_id":VIEWMANAGER.currentUser.bTCSubrUserID,
            "req_id":model.reqId
        ]
        
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: DELETE_BTC_REQUEST_API, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: "Request deleted successfully!")
            self.arrRequests.removeObject(at: indexPath.row)
            self.tblView.reloadData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }

    }
    
    //MARK: - Table view datasource & delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = arrRequests.count > 50 ? nil : tableBGView()
        return arrRequests.count
    }
    
    func tableBGView() -> UILabel
    {
        let lblBG = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 30))
        lblBG.font = UIFont.appRegularFont(size: 18)
        lblBG.text = "No request found"
        lblBG.textAlignment = .center
        return lblBG
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyWalletCell") as! MyWalletCell
        cell.backgroundColor = indexPath.row % 2 == 0 ? .white : UIColor(red: 0.96, green: 0.95, blue: 0.95, alpha: 1.0)
        let model = arrRequests[indexPath.row] as! MyBTCRequestModel
        cell.lblSrNo.text = "\(indexPath.row + 1)"
        cell.lblDate.text = model.rDate
        cell.lblBuySell.text = model.rType
        cell.lblBtc.text = "\(model.rBTC!)BTC"
        cell.indexPath = indexPath
        cell.onDeleteClicked { (indexPath) in
            
            CustomAlertView.showAlertWithYesNo(title: "Delete?", messsage: "Are you sure you want to delete this request?", completion: {
                self.deleteMyRequestAPICall(indexPath: indexPath)
            })
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 35))
        header.backgroundColor = UIColor.appThemeColor
        
        let lblSrNo = UILabel(frame: CGRect(x: 0, y: 0, width: 40, height: header.frame.size.height))
        lblSrNo.font = UIFont.appRegularFont(size: 16)
        lblSrNo.textColor = .white
        lblSrNo.text = "No"
        lblSrNo.textAlignment = .center
        header.addSubview(lblSrNo)
        
        let separator1 = UIView(frame: CGRect(x: lblSrNo.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator1.backgroundColor = .white
        header.addSubview(separator1)
        
        let lblDate = UILabel(frame: CGRect(x: lblSrNo.frame.maxX, y: 0, width: (header.frame.size.width - 40)/4, height: header.frame.size.height))
        lblDate.font = lblSrNo.font
        lblDate.textColor = .white
        lblDate.text = "Date"
        lblDate.textAlignment = .center
        header.addSubview(lblDate)
        
        let separator2 = UIView(frame: CGRect(x: lblDate.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator2.backgroundColor = .white
        header.addSubview(separator2)
        
        let lblName = UILabel(frame: CGRect(x: lblDate.frame.maxX, y: 0, width: lblDate.frame.size.width, height: header.frame.size.height))
        lblName.font = lblSrNo.font
        lblName.textColor = .white
        lblName.text = "Buy/Sell"
        lblName.textAlignment = .center
        header.addSubview(lblName)
        
        let separator3 = UIView(frame: CGRect(x: lblName.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator3.backgroundColor = .white
        header.addSubview(separator3)
        
        let lblBTC = UILabel(frame: CGRect(x: lblName.frame.maxX, y: 0, width: lblDate.frame.size.width, height: header.frame.size.height))
        lblBTC.font = lblSrNo.font
        lblBTC.textColor = .white
        lblBTC.text = "BTC"
        lblBTC.textAlignment = .center
        header.addSubview(lblBTC)
        
        let separator4 = UIView(frame: CGRect(x: lblBTC.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator4.backgroundColor = .white
        header.addSubview(separator4)
        
        let lblDistance = UILabel(frame: CGRect(x: lblBTC.frame.maxX, y: 0, width: lblDate.frame.size.width, height: header.frame.size.height))
        lblDistance.font = lblSrNo.font
        lblDistance.textColor = .white
        lblDistance.text = "Action"
        lblDistance.textAlignment = .center
        header.addSubview(lblDistance)

        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    //MARK: - Button Clicked
    
    @IBAction func btnShowProfilePicClicked(_ sender: Any) {
        let imgViewerVC = ImageViewerController()
        imgViewerVC.imgURL = VIEWMANAGER.currentUser.profilePic
        imgViewerVC.modalPresentationStyle = .custom
        imgViewerVC.modalTransitionStyle = .crossDissolve
        self.present(imgViewerVC, animated: true, completion: nil)

    }
    
    @IBAction func btnShareClicked(_ sender: Any) {
        let myShare = "\(VIEWMANAGER.currentUser.fName!) \(VIEWMANAGER.currentUser.lName!)\nAccept payments in BTC. Please enter these phone number \(VIEWMANAGER.currentUser.mobile!) with your BTC app to transfer BTC to me.\n\n Download BTC App from Play store or App Store and get assured 1% growth per day on your investment. Click here \(APP_STORE_LINK)"
        let image: UIImage = UIImage(named: "logo")!
        
        let shareVC: UIActivityViewController = UIActivityViewController(activityItems: [image, myShare], applicationActivities: nil)
        shareVC.excludedActivityTypes = [.print, .postToWeibo,.addToReadingList,.postToVimeo]

        self.present(shareVC, animated: true, completion: nil)

    }
    
    
    //MARK: - Helper
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height) {
            if loadStarted {
                getMyPostRequestAPICall(startIdx: start_index, endIndex: end_index,showLoader: false)
                loadStarted = false
            }
        }
    }
    
    override func btnBackClicked(_ sender: Any) {
        if fromBuySellBTC {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    deinit {
        tblView.removeObserver(self, forKeyPath: "contentSize", context: nil)
    }
}

