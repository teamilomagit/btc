//
//  BuyerSellerDetailsController.swift
//  BTS
//
//  Created by Pawan Ramteke on 31/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import SDWebImage
class BuyerSellerDetailsController: BaseViewController,UITextFieldDelegate{

    var scrTitle : String!
    var reqID : String!
    @IBOutlet weak var imgViewDP: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    
    @IBOutlet weak var txtMobileNo: UITextView!
    @IBOutlet weak var lblReqDate: UILabel!
    @IBOutlet weak var lblReqBTC: UILabel!
    @IBOutlet weak var lblReqLocation: UILabel!
    var detailsModel : BuyerSellerDetailsModel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.headerView.lblHeaderTitle.text = scrTitle

        imgViewDP.layer.borderWidth = 2.0
        imgViewDP.layer.borderColor = UIColor.appThemeColor.cgColor
        self.headerView.lblHeaderTitle.text = scrTitle
        getRequestDetailsAPICall()
    }
    
    func getRequestDetailsAPICall()
    {
        let param = [
            "user_id" : VIEWMANAGER.currentUser.bTCSubrUserID,
            "req_id" : reqID
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_BUYER_SELLER_DETAILS_API, params: param as [String : AnyObject], completion: { (respose) in
            VIEWMANAGER.hideActivityIndicator()
            self.detailsModel = respose as! BuyerSellerDetailsModel
            self.setupUserData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
    }
    
    func setupUserData()
    {
        lblName.text = "\(detailsModel.fName!) \(detailsModel.lName!)"
        lblMobileNo.text = detailsModel.mobile
        txtMobileNo.text = detailsModel.mobile
        lblReqDate.text = detailsModel.rDate
        lblReqBTC.text = "\(detailsModel.bTC!) BTC"
        lblReqLocation.text = detailsModel.address == "" ? "-" : detailsModel.address
        
        imgViewDP.sd_setImage(with: URL(string:detailsModel.profilePic), placeholderImage: UIImage(named:"ic_profile"), options: SDWebImageOptions.allowInvalidSSLCertificates, completed: nil)
        
    }
    
    @IBAction func btnShowProfilePicClicked(_ sender: Any) {
        let imgViewerVC = ImageViewerController()
        imgViewerVC.imgURL = detailsModel.profilePic
        imgViewerVC.modalPresentationStyle = .custom
        imgViewerVC.modalTransitionStyle = .crossDissolve
        self.present(imgViewerVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnCallNowClicked(_ sender: Any) {
        
        let callURL = URL(string: "tel://\(detailsModel.mobile!)")

        if callURL != nil && UIApplication.shared.canOpenURL(callURL!) {
            
            UIApplication.shared.open(callURL!, options: [:], completionHandler: nil)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}


