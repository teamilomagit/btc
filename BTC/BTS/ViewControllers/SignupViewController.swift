//
//  SignupViewController.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class SignupViewController: BaseViewController {
    
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var lblTItle: UILabel!

    @IBOutlet weak var btnAlreadyHaveAcc: UIButton!
    
    
    @IBOutlet weak var txtFieldFirstName: MaterialTextField!
    @IBOutlet weak var txtFieldLastName: MaterialTextField!
    @IBOutlet weak var txtFieldMobileNo: MaterialTextField!
    @IBOutlet weak var txtFieldPassword: MaterialTextField!
    @IBOutlet weak var txtFieldConfirmPassword: MaterialTextField!
    
    var gradient : CAGradientLayer!

    //MARK: -
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTItle.textColor = .white
        lblTItle.font = UIFont.appBoldFont(size: 16)
        btnAlreadyHaveAcc.setTitleColor(.appThemeColor, for: .normal)
        btnAlreadyHaveAcc.titleLabel?.font = UIFont.appRegularFont(size: 16)
        
        txtFieldMobileNo.setTextFieldTag(tag: .TAG_MOBILE)
        txtFieldPassword.setTextFieldTag(tag: .TAG_PIN_NUMBER)
        txtFieldConfirmPassword.setTextFieldTag(tag: .TAG_PIN_NUMBER)
        setupHeader()
    }
    
    func setupHeader()
    {
        gradient = CAGradientLayer()
        gradient.frame = header.bounds
        gradient.colors = [UIColor.white.withAlphaComponent(1.0).cgColor,UIColor.appThemeColor.cgColor]
        header.layer.insertSublayer(gradient, at: 0)
    }
    
    //MARK: - API Call
    func registrationAPICall()
    {
        let param : [String : AnyObject] = [
            "fname":txtFieldFirstName.text as AnyObject,
            "lname":txtFieldLastName.text as AnyObject,
            "phone":txtFieldMobileNo.text as AnyObject,
            "pass":txtFieldPassword.text as AnyObject
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: REGISTRATION_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            let appDel = UIApplication.shared.delegate as! AppDelegate
            appDel.setDashboardAsRoot()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }

    }

    //MARK: - Button Clicked Events
    
    @IBAction func btnRegisterClicked(_ sender: Any) {
        if validate() {
            self.view.endEditing(true)
            registrationAPICall()
        }
    }
    
    @IBAction func btnAlreadyHaveAccClicked(_ sender: Any) {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
        self.navigationController?.pushViewController(loginVC!, animated: true)
    }
    
    
    //MARK: - Helper
    func validate() -> Bool
    {
        if txtFieldFirstName.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter first name")
            return false
        }
        if txtFieldLastName.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(message: "Please enter last name")
            return false
        }
        if (txtFieldMobileNo.text?.trimString().count)! <  10 {
            VIEWMANAGER.showToast(message: "Please enter valid mobile number")
            return false
        }
        if (txtFieldPassword.text?.trimString().count)! < 6 {
            VIEWMANAGER.showToast(message: "Please enter 6 digit pin")
            return false
        }
        if txtFieldConfirmPassword.text != txtFieldPassword.text {
            VIEWMANAGER.showToast(message: "Password does not match")
            return false
        }
        return true
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradient.frame = header.bounds
    }
}
