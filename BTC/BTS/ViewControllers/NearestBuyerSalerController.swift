//
//  NearestBuyerSalerController.swift
//  BTS
//
//  Created by Pawan Ramteke on 22/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import CoreLocation

class NearestBuyerSalerController: BaseViewController,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var lblBTitle: UILabel!
    var isBuyerList : Bool!
    
    let page_size : Int = 15
    var start_index : Int = 0
    var end_index : Int = 15
    var loadStarted : Bool = false

    var currentLocation : CLLocation!
    var locationManager: CLLocationManager!
    var currentAddrss:String!
    var arrList = NSMutableArray()
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerView.lblHeaderTitle.text = isBuyerList ? "Nearest Buyer List" : "Nearest Seller List"
        
        lblBTitle.text = self.headerView.lblHeaderTitle.text
        tblView.separatorStyle = .none
        tblView.register( UINib(nibName: "NearestBuyerCell", bundle: nil), forCellReuseIdentifier: "NearestBuyerCell")
        VIEWMANAGER.showActivityIndicator()
        initializeLocationManager()
    }
    
    //MARK: - Location manager
    func initializeLocationManager()
    {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        else{
            VIEWMANAGER.showToast(message: "Location is not enabled")
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if currentLocation == nil {
            getLocationAddress(location: manager.location!)
        }
        currentLocation = manager.location
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        CustomAlertView.showPermissionEnableAlert(title: "Location Disabled", message: "To send location, go to your Settings\nApp > Privacy > Location Services", completion: {
            let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/")
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            }
        })
        VIEWMANAGER.hideActivityIndicator()
    }
    
    func getLocationAddress(location:CLLocation)
    {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location) { (placeMarks, error) in
            if error == nil && placeMarks!.count > 0
            {
                let placeMark  = placeMarks?.last as! CLPlacemark
                print(self.getAddress(placemark: placeMark))
                let address = self.getAddress(placemark: placeMark)
                VIEWMANAGER.hideActivityIndicator()
                self.currentAddrss = address
                self.getBuyerSellerListAPICall(address: address, startIdx: self.start_index, endIndex: self.end_index)
            }
        }
    }
    
    func getBuyerSellerListAPICall(address:String,startIdx:Int,endIndex:Int,showLoader:Bool = true)
    {
        let param = [
                        "user_id":VIEWMANAGER.currentUser.bTCSubrUserID,
                        "lat":currentLocation.coordinate.latitude,
                        "lng":currentLocation.coordinate.longitude,
                        "address":address,
                        "start_index":startIdx,
                        "end_index":endIndex
            ] as [String : Any]
        if showLoader {
            VIEWMANAGER.showActivityIndicator()
        }
        RemoteAPI().callPOSTApi(apiName: isBuyerList ? GET_NEAREST_BUYER_API : GET_NEAREST_SELLER_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            
            let arrData = responseData as! NSArray
            
            self.arrList.addObjects(from: responseData as! [Any])
            self.tblView.reloadData()
            
            if arrData.count == self.page_size
            {
                self.start_index = self.arrList.count
                self.end_index = self.end_index + self.page_size
                self.loadStarted = true
                self.loader.startAnimating()
            }
            else{
                self.loadStarted = false
                self.loader.stopAnimating()
            }
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    
    //MARK: - Table Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = arrList.count > 0 ? nil : tableBGView()
        return arrList.count
    }
    
    func tableBGView() -> UILabel
    {
        let lblBG = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 50))
        lblBG.font = UIFont.appRegularFont(size: 16)
        lblBG.textAlignment = .center
        lblBG.text = "No record found"
        return lblBG
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NearestBuyerCell") as! NearestBuyerCell
        cell.backgroundColor = indexPath.row % 2 == 0 ? .white : UIColor(red: 0.96, green: 0.95, blue: 0.95, alpha: 1.0)
        
        let model = arrList[indexPath.row]

        isBuyerList ? configureCellForBuyer(model: model as! NearestBuyerModel, cell : cell) : configureCellForSeller(model: model as! NearestSellerModel, cell : cell)
        cell.indexPath = indexPath
        cell.lblSrNo.text = "\(indexPath.row + 1)"

        cell.onInfoButtonClicked { (indexPath) in
            self.isBuyerList ? self.pushToBuyerDetailsController(indexPath) : self.pushToSellerDetailsController(indexPath)
        }
        return cell
    }
    
    func configureCellForBuyer(model : NearestBuyerModel , cell : NearestBuyerCell)
    {
        cell.lblDate.text = model.bDate
        cell.lblBtc.text = "\(model.bTC!) BTC"
        cell.lblDistance.text = "\(model.distance!) KM"//"\(getDistance(latitude: model.bLat, longitude: model.bLng)) KM"
    }
    
    func configureCellForSeller(model : NearestSellerModel , cell : NearestBuyerCell)
    {
        cell.lblDate.text = model.sDate
        cell.lblBtc.text = "\(model.bTC!) BTC"
        cell.lblDistance.text = "\(model.distance!) KM"//"\(getDistance(latitude: model.sLat, longitude: model.sLng)) KM"
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 35))
        header.backgroundColor = UIColor.appThemeColor
        
        let lblSrNo = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: header.frame.size.height))
        lblSrNo.font = UIFont.appRegularFont(size: 16)
        lblSrNo.textColor = .white
        lblSrNo.text = "No."
        lblSrNo.textAlignment = .center
        header.addSubview(lblSrNo)
        
        let separator1 = UIView(frame: CGRect(x: lblSrNo.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator1.backgroundColor = .white
        header.addSubview(separator1)
        
        let lblDate = UILabel(frame: CGRect(x: lblSrNo.frame.maxX, y: 0, width: (header.frame.size.width - 90)/3, height: header.frame.size.height))
        lblDate.font = lblSrNo.font
        lblDate.textColor = .white
        lblDate.text = "Date"
        lblDate.textAlignment = .center
        header.addSubview(lblDate)
        
        let separator2 = UIView(frame: CGRect(x: lblDate.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator2.backgroundColor = .white
        header.addSubview(separator2)
        
        let lblBTC = UILabel(frame: CGRect(x: lblDate.frame.maxX, y: 0, width: lblDate.frame.size.width, height: header.frame.size.height))
        lblBTC.font = lblSrNo.font
        lblBTC.textColor = .white
        lblBTC.text = "BTC"
        lblBTC.textAlignment = .center
        header.addSubview(lblBTC)
        
        let separator3 = UIView(frame: CGRect(x: lblBTC.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator3.backgroundColor = .white
        header.addSubview(separator3)
        
        let lblDistance = UILabel(frame: CGRect(x: lblBTC.frame.maxX, y: 0, width: lblDate.frame.size.width, height: header.frame.size.height))
        lblDistance.font = lblSrNo.font
        lblDistance.textColor = .white
        lblDistance.text = "Distance"
        lblDistance.textAlignment = .center
        header.addSubview(lblDistance)
        
        let separator4 = UIView(frame: CGRect(x: lblDistance.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator4.backgroundColor = .white
        header.addSubview(separator4)
        
        let lblDetails = UILabel(frame: CGRect(x: lblDistance.frame.maxX, y: 0, width: 60, height: 40))
        lblDetails.font = lblSrNo.font
        lblDetails.textColor = .white
        lblDetails.text = "Details"
        lblDetails.textAlignment = .center
        header.addSubview(lblDetails)

        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }

    
    //MARK: - Helper
    
    func pushToBuyerDetailsController(_ indexPath:IndexPath)
    {
        let model = arrList[indexPath.row] as! NearestBuyerModel

        let infoDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "BuyerSellerDetailsController") as! BuyerSellerDetailsController
        infoDetailsVC.reqID = model.bReqId
        self.navigationController?.pushViewController(infoDetailsVC, animated: true)

    }
    
    func pushToSellerDetailsController(_ indexPath:IndexPath)
    {
        let model = arrList[indexPath.row] as! NearestSellerModel
        
        let infoDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "BuyerSellerDetailsController") as! BuyerSellerDetailsController
        infoDetailsVC.reqID = model.sReqId
        infoDetailsVC.scrTitle = isBuyerList ? "Buyer Details" : "Seller Details"
        self.navigationController?.pushViewController(infoDetailsVC, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tblView.contentOffset.y >= (tblView.contentSize.height - tblView.frame.size.height) {
            if loadStarted {
                getBuyerSellerListAPICall(address: currentAddrss, startIdx: start_index, endIndex: end_index,showLoader: false)
                loadStarted = false
            }
        }
    }
    
    
    func getAddress(placemark:CLPlacemark) -> String
    {
        var strAdd : String = ""
        if (placemark.subThoroughfare != nil && placemark.subThoroughfare?.count != 0){
            strAdd = placemark.subThoroughfare!
        }
        
        if (placemark.thoroughfare != nil && placemark.thoroughfare?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.thoroughfare!)
            }
            else{
                strAdd = placemark.thoroughfare!
            }
        }
        
        if (placemark.subLocality != nil && placemark.subLocality?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.subLocality!)
            }
            else{
                strAdd = placemark.subLocality!;
            }
        }
        
        if (placemark.locality != nil && placemark.locality?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.locality!)
            }
            else{
                strAdd = placemark.locality!;
            }
        }
        
        if (placemark.administrativeArea != nil && placemark.administrativeArea?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.administrativeArea!)
            }
            else{
                strAdd = placemark.administrativeArea!;
            }
        }
        
        if (placemark.country != nil && placemark.country?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.country!)
            }
            else{
                strAdd = placemark.country!;
            }
        }
        
        if (placemark.postalCode != nil && placemark.postalCode?.count != 0)
        {
            if (strAdd.count != 0){
                strAdd = String(format: "%@, %@",strAdd,placemark.postalCode!)
            }
            else{
                strAdd = placemark.postalCode!
            }
        }
        
        return strAdd
    }
    
    func getDistance(latitude:String,longitude:String) -> String
    {
        let loc = CLLocation(latitude: Double(latitude)!, longitude: Double(longitude)!)
        
        let distance = currentLocation.distance(from: loc)
        
        let kilometers = distance / 1000.0
        
        return String(format:"%.1f",kilometers)
    }
}
