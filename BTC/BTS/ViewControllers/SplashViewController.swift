//
//  SplashViewController.swift
//  BTS
//
//  Created by Pawan Ramteke on 28/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    
    @IBOutlet weak var imgViewLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        rotateView(targetView: imgViewLogo, duration: 1.0)
    }

    private func rotateView(targetView: UIView, duration: Double = 1.0) {
        UIView.animate(withDuration: duration, delay: 0.5, options: .curveLinear, animations: {
            targetView.transform = targetView.transform.rotated(by: CGFloat(Double.pi))
            
        }) { finished in
            
            UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
                targetView.transform = targetView.transform.rotated(by: CGFloat(Double.pi))

            }) { finished in
                let app_Del = UIApplication.shared.delegate as! AppDelegate
                app_Del.checkForLogin()
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
