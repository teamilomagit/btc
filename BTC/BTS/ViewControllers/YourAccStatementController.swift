//
//  YourAccStatementController.swift
//  BTS
//
//  Created by Pawan Ramteke on 22/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class YourAccStatementController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblView: UITableView!
    
    let page_size : Int = 15
    var start_index : Int = 0
    var end_index : Int = 15

    let arrAccStmt = NSMutableArray()
    var loadStarted : Bool = false
    @IBOutlet weak var viewLoadMore: UIView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    var fromTransferSuccess : Bool = false
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.headerView.lblHeaderTitle.text = "Your Account Statement"
       
        tblView.separatorStyle = .none
        tblView.register( UINib(nibName: "AccountStmtCell", bundle: nil), forCellReuseIdentifier: "AccountStmtCell")
        
        getAccountStatementAPICall(startIdx: start_index, endIndex: end_index)
        
        self.headerView.onBackClicked {
            if self.fromTransferSuccess == true {
                self.navigationController?.popToRootViewController(animated: true)
            }
            else{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
     //MARK: - API Call
    
    func getAccountStatementAPICall(startIdx:Int,endIndex:Int,showLoader:Bool = true)
    {
        let param = [
                    "user_id":VIEWMANAGER.currentUser.bTCSubrUserID,
                    "start_index":startIdx,
                    "end_index":endIndex
            ] as [String : Any]
        
        if showLoader {
            VIEWMANAGER.showActivityIndicator()
        }
        RemoteAPI().callPOSTApi(apiName: GET_ACCOUNT_STATEMENT_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            
            let arrData = responseData as! NSArray
            
            self.arrAccStmt.addObjects(from: responseData as! [Any])
            self.tblView.reloadData()

            if arrData.count == self.page_size
            {
                self.start_index = self.arrAccStmt.count
                self.end_index = self.end_index + self.page_size
                self.loadStarted = true
                self.loader.startAnimating()
            }
            else{
                self.loadStarted = false
                self.loader.stopAnimating()
            }
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }

    }
    
    
    //MARK: - table view Datasource & delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = arrAccStmt.count == 0 ? tableBGView() : nil
        return arrAccStmt.count
    }
    
    func tableBGView() -> UILabel
    {
        let lblBG = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 50))
        lblBG.font = UIFont.appRegularFont(size: 16)
        lblBG.textAlignment = .center
        lblBG.text = "No record found"
        return lblBG
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountStmtCell") as! AccountStmtCell
        cell.backgroundColor = indexPath.row % 2 == 0 ? .white : UIColor(red: 0.96, green: 0.95, blue: 0.95, alpha: 1.0)
        
        let model = arrAccStmt[indexPath.row] as! MyAccStmtModel
        cell.lblSrNo.text = "\(indexPath.row + 1)"
        cell.lblDate.text = formattedDate(strDate: model.tDate)
        cell.lblType.text = model.tType
        cell.lblValue.text = "\(model.tBTC!) BTC"
        cell.lblBalance.text = "\(model.tBalance!) BTC"
        cell.indexPath = indexPath
        
        cell.onInfoButtonClicked { (indexPatg) in
            self.pushToInfoDetailsController(indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
        header.backgroundColor = UIColor.appThemeColor
        
        let lblSrNo = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: header.frame.size.height))
        lblSrNo.font = UIFont.appRegularFont(size: 16)
        lblSrNo.textColor = .white
        lblSrNo.text = "No"
        lblSrNo.textAlignment = .center
        header.addSubview(lblSrNo)
        
        let separator1 = UIView(frame: CGRect(x: lblSrNo.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator1.backgroundColor = .white
        header.addSubview(separator1)
        
        let lblDate = UILabel(frame: CGRect(x: lblSrNo.frame.maxX, y: 0, width: (header.frame.size.width - 30)/5, height: header.frame.size.height))
        lblDate.font = lblSrNo.font
        lblDate.textColor = .white
        lblDate.text = "Date"
        lblDate.textAlignment = .center
        header.addSubview(lblDate)
        
        let separator2 = UIView(frame: CGRect(x: lblDate.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator2.backgroundColor = .white
        header.addSubview(separator2)
        
        let lblType = UILabel(frame: CGRect(x: lblDate.frame.maxX, y: 0, width: lblDate.frame.size.width, height: header.frame.size.height))
        lblType.font = lblSrNo.font
        lblType.textColor = .white
        lblType.text = "Type"
        lblType.textAlignment = .center
        header.addSubview(lblType)
        
        let separator3 = UIView(frame: CGRect(x: lblType.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator3.backgroundColor = .white
        header.addSubview(separator3)
        
        let lblValue = UILabel(frame: CGRect(x: lblType.frame.maxX, y: 0, width: lblDate.frame.size.width, height: header.frame.size.height))
        lblValue.font = lblSrNo.font
        lblValue.textColor = .white
        lblValue.text = "Value"
        lblValue.textAlignment = .center
        header.addSubview(lblValue)
        
        
        let separator4 = UIView(frame: CGRect(x: lblValue.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator4.backgroundColor = .white
        header.addSubview(separator4)
        
        let lblBTC = UILabel(frame: CGRect(x: lblValue.frame.maxX, y: 0, width: lblDate.frame.size.width, height: header.frame.size.height))
        lblBTC.font = lblSrNo.font
        lblBTC.textColor = .white
        lblBTC.text = "Balance"
        lblBTC.textAlignment = .center
        header.addSubview(lblBTC)
        
        let separator5 = UIView(frame: CGRect(x: lblBTC.frame.maxX, y: 5, width: 1, height: header.frame.size.height - 10))
        separator5.backgroundColor = .white
        header.addSubview(separator5)
        
        let lblBalance = UILabel(frame: CGRect(x: lblBTC.frame.maxX, y: 0, width: lblDate.frame.size.width, height: header.frame.size.height))
        lblBalance.font = lblSrNo.font
        lblBalance.textColor = .white
        lblBalance.text = "Details"
        lblBalance.textAlignment = .center
        header.addSubview(lblBalance)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    //MARK: - Helper
    
    func pushToInfoDetailsController(_ indexPath:IndexPath)
    {
        let model = arrAccStmt[indexPath.row] as! MyAccStmtModel

        let infoDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "StatementDetailsController") as! StatementDetailsController
        infoDetailsVC.transID = model.transID
        self.navigationController?.pushViewController(infoDetailsVC, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tblView.contentOffset.y >= (tblView.contentSize.height - tblView.frame.size.height) {
            if loadStarted {
                getAccountStatementAPICall(startIdx: start_index, endIndex: end_index,showLoader: false)
                loadStarted = false
            }
        }

    }
    
    func formattedDate(strDate:String) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let date = formatter.date(from: strDate)
        
        formatter.dateFormat = "dd MMM yy"
        let newDate = formatter.string(from: date!)
        return newDate
        
    }
}
