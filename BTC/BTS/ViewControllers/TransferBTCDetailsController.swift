//
//  TransferBTCDetailsController.swift
//  BTS
//
//  Created by Pawan Ramteke on 26/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class TransferBTCDetailsController: BaseViewController,UITextFieldDelegate {

    
    @IBOutlet weak var lblName: MaterialTextField!
    @IBOutlet weak var lblCity: MaterialTextField!
    @IBOutlet weak var lblMobile: MaterialTextField!
    @IBOutlet weak var lblNoOfBTC: MaterialTextField!
    
    @IBOutlet weak var lblInrValue: UILabel!
    
    var userModel : UserModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerView.lblHeaderTitle.text = "Transfer BTC"
        lblName.text = userModel.sName
        //lblCity.text = userModel.sCity
        lblMobile.text = userModel.sPhone
        
        lblNoOfBTC.delegate = self
        lblNoOfBTC.font = UIFont.appBoldFont(size: 22)
        lblNoOfBTC.textColor = .red
    }

    func transferBTCApiCall()
    {
        let param = [
            "user_id":VIEWMANAGER.currentUser.bTCSubrUserID,
            "subID":userModel.subID,
            "btcNo":lblNoOfBTC.text
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: TRANSFER_BTC_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: responseData as? String ?? "")
            self.getBTCRateAPICall()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    func getBTCRateAPICall()
    {
        VIEWMANAGER.getBTCRateAPICall {
            self.headerView.lblBtcRate.text = "1 BTC = \(String(describing: VIEWMANAGER.rateModel.btcRate!)) INR"
            if VIEWMANAGER.rateModel.btcAccBalance != nil
            {
                self.headerView.lblAccBalance.text = "\(String(describing: VIEWMANAGER.rateModel.btcAccBalance!)) BTC"
            }
            
            self.pushToAccountStatementController()
        }
    }
    
    @IBAction func btnTransferNowClicked(_ sender: Any) {
        
        if lblNoOfBTC.text?.count == 0 {
            VIEWMANAGER.showToast(message: "Please enter Number of BTC")
            return
        }
        
        let balance = NumberFormatter().number(from: VIEWMANAGER.rateModel.btcAccBalance!)
        if  Int(truncating: balance!)  < Int(lblNoOfBTC.text!)!{
            //VIEWMANAGER.showToast(message: "Insufficient Balance !!!!")
            CustomAlertView.showAlert(withTitle: "Insufficient Balance !!!!", messsage: "Your acount balancce is not sufficient to transfer the number of BTC you have entered.", completion: nil)
            return
        }
       
        transferBTCApiCall()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,let textRange = Range(range, in: text)
        {
            let updatedText = text.replacingCharacters(in: textRange,with: string)
            let noOfBTC =  updatedText == "" ? 0 : NumberFormatter().number(from: updatedText)!.floatValue
            let currentRate = NumberFormatter().number(from: VIEWMANAGER.rateModel.btcRate!)!.floatValue
            
            let rate = noOfBTC * currentRate
            
            lblInrValue.text = NSString(format: "%.1f INR", rate) as String
            
        }
        return true
    }
    
    func pushToAccountStatementController()
    {
        let myAccStmtVC = self.storyboard?.instantiateViewController(withIdentifier: "YourAccStatementController") as! YourAccStatementController
        myAccStmtVC.fromTransferSuccess = true
        self.navigationController?.pushViewController(myAccStmtVC, animated: true)

    }
}
