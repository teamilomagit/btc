//
//  WebViewController.swift
//  BTS
//
//  Created by Pawan Ramteke on 25/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class WebViewController: BaseViewController,UIWebViewDelegate {

    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView!
    var urlToLoad : String!
    var navTitle : String!
    @IBOutlet weak var navHtConstr: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
       // navHtConstr.constant = SCREEN_HEIGHT > 568 ? 80 : 60
        self.lblNavTitle.text = navTitle
        webView.loadRequest(URLRequest(url: URL(string:urlToLoad)!))
        webView.delegate = self
        webView.scrollView.bounces = false
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        loader.stopAnimating()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
