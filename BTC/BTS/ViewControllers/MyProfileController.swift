//
//  MyProfileController.swift
//  BTS
//
//  Created by Pawan Ramteke on 26/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import SDWebImage
class MyProfileController: BaseViewController {

    
    @IBOutlet weak var imgViewDP: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var txtFieldAccMobNo: MaterialTextField!
    @IBOutlet weak var txtFieldAccNo: MaterialTextField!
    @IBOutlet weak var txtFieldAccEmailId: MaterialTextField!
    
    @IBOutlet weak var txtFieldDob: UITextField!
    @IBOutlet weak var txtFieldGender: UITextField!
    @IBOutlet weak var txtFieldOccupation: UITextField!
    @IBOutlet weak var txtFieldIncomeRange: UITextField!
    
    @IBOutlet weak var txtFieldAddress: MaterialTextField!
    @IBOutlet weak var txtFieldLandmark: MaterialTextField!
    @IBOutlet weak var txtFieldPinCode: MaterialTextField!
    @IBOutlet weak var txtFieldCity: MaterialTextField!
    @IBOutlet weak var txtFieldState: MaterialTextField!
    
    var myProfileModel : MyProfileModel!
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        imgViewDP.layer.borderWidth = 2.0
        imgViewDP.layer.borderColor = UIColor.appThemeColor.cgColor
        imgViewDP.sd_setImage(with: URL(string:VIEWMANAGER.currentUser.profilePic), placeholderImage: UIImage(named:"ic_profile"), options: SDWebImageOptions.allowInvalidSSLCertificates, completed: nil)

        lblUserName.text = "\(VIEWMANAGER.currentUser.fName!) \(VIEWMANAGER.currentUser.lName!)"
        lblMobileNo.text = VIEWMANAGER.currentUser.mobile
        
        getMyProfileAPICall()
    }
    
    // MARK: - API Call
    
    func getMyProfileAPICall()
    {
        let param : [String : AnyObject] = [
            "user_id":VIEWMANAGER.currentUser.bTCSubrUserID as AnyObject
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: MY_PROFILE_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.myProfileModel = responseData as! MyProfileModel
            self.setupProfileData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
    }
    
    func uploadProfileImage(imagePath:String)
    {
        let param  = [
            "user_id":VIEWMANAGER.currentUser.bTCSubrUserID,
            "profImgfile":imagePath
            ] as [String : Any]
        
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().uploadProfileImage(apiName: CHANGE_PROFILE_PIC_API, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            
            let responseDict = NSMutableDictionary(dictionary: Preferences.getLoginData() as! NSDictionary)
            responseDict.setValue(response as! String, forKey: "profilePic")
            Preferences.saveLoginData(response: responseDict)
            VIEWMANAGER.currentUser.profilePic = response as! String
            VIEWMANAGER.showToast(message: "Profile upload Successfully!!!")
            SDImageCache.shared().removeImage(forKey: VIEWMANAGER.currentUser.profilePic, withCompletion: {
                self.imgViewDP.image = UIImage(named:"ic_profile")
                self.imgViewDP.sd_setImage(with: URL(string:VIEWMANAGER.currentUser.profilePic), placeholderImage: UIImage(named:"ic_profile"), options: SDWebImageOptions.allowInvalidSSLCertificates, completed: nil)
            })
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ProfileUpdateNotificcation"), object: nil)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
    }

    
    func setupProfileData()
    {
        lblUserName.text = "\(myProfileModel.fName!) \(myProfileModel.lName!)"

        txtFieldAccMobNo.text = myProfileModel.mobile == "" ? "-" : myProfileModel.mobile
        txtFieldAccEmailId.text = myProfileModel.eMail == "" ? "-" : myProfileModel.eMail
        txtFieldAccNo.text = myProfileModel.bTCSubrNo == "" ? "-" : myProfileModel.bTCSubrNo
        
        txtFieldDob.text = myProfileModel.dob == "" ? "-" : myProfileModel.dob
        txtFieldGender.text = myProfileModel.gender == "" ? "-" : myProfileModel.gender
        txtFieldOccupation.text = myProfileModel.occupation == "" ? "-" : myProfileModel.occupation
        txtFieldIncomeRange.text = myProfileModel.incomeRange == "" ? "-" : myProfileModel.incomeRange
        
        txtFieldAddress.text = myProfileModel.addressLine == "" ? "-" : myProfileModel.addressLine
        txtFieldLandmark.text = myProfileModel.landmark == "" ? "-" : myProfileModel.landmark
        txtFieldPinCode.text = myProfileModel.pincode == "" ? "-" : myProfileModel.pincode
        txtFieldCity.text = myProfileModel.city == "" ? "-" : myProfileModel.city
        txtFieldState.text = myProfileModel.state == "" ? "-" : myProfileModel.state
    }
    
    // MARK: - Button Clicked Events
    
    @IBAction func btnChangeProfilePicClicked(_ sender: Any) {
        VIEWMANAGER.showImagePicker { (image,imgPath) in
            self.imgViewDP.image = image
            self.uploadProfileImage(imagePath: imgPath)
        }
    }
    
    @IBAction func btnEditAccountClicked(_ sender: Any) {
        pushToEditProfileVC(editType: "Account")
    }
    
    @IBAction func btnEditPersonalInfoClicked(_ sender: Any) {
        pushToEditProfileVC(editType: "Personal Info")
    }
    
    @IBAction func btnEditAddressInfoClicked(_ sender: Any) {
        pushToEditProfileVC(editType: "")
    }
    
    func pushToEditProfileVC(editType:String)
    {
        let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileController") as! EditProfileController
        editProfileVC.editType = editType
        editProfileVC.profileModel = myProfileModel
        self.navigationController?.pushViewController(editProfileVC, animated: true)
        
        editProfileVC.onUpdateProfile {
            self.getMyProfileAPICall()
        }
    }
    
    @IBAction func btnShowProfilePicClicked(_ sender: Any) {
        let imgViewerVC = ImageViewerController()
        imgViewerVC.imgURL = VIEWMANAGER.currentUser.profilePic
        imgViewerVC.modalPresentationStyle = .custom
        imgViewerVC.modalTransitionStyle = .crossDissolve
        self.present(imgViewerVC, animated: true, completion: nil)
    }
    
}
