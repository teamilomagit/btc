//
//  ResetPasswordController.swift
//  BTS
//
//  Created by Pawan Ramteke on 25/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class ResetPasswordController: BaseViewController {

    var mobileNo : String!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var lblTItle: UILabel!
    var gradient : CAGradientLayer!
    @IBOutlet weak var txtFieldNewPassword: MaterialTextField!
    @IBOutlet weak var txtFieldConfirmPass: MaterialTextField!
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTItle.textColor = .white
        lblTItle.font = UIFont.appBoldFont(size: 16)
        txtFieldNewPassword.setTextFieldTag(tag: .TAG_PIN_NUMBER)
        txtFieldConfirmPass.setTextFieldTag(tag: .TAG_PIN_NUMBER)
        setupHeader()
    }

    func setupHeader()
    {
        gradient = CAGradientLayer()
        gradient.frame = header.bounds
        gradient.colors = [UIColor.white.withAlphaComponent(1.0).cgColor,UIColor.appThemeColor.cgColor]
        header.layer.insertSublayer(gradient, at: 0)
    }
    
    
    // MARK: - API Call
    func resetPasswrdAPICall()
    {
        let param = [
                        "txtPhoneNumber" : mobileNo,
                        "txtNewPassword" : txtFieldNewPassword.text!,
                        "type" : "Reset"
            ] as [String : AnyObject]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: RESET_PASSWORD_API, params: param, completion: { (reseposeData) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: reseposeData as! String)
            let  vc =  self.navigationController?.viewControllers.filter({$0 is LoginViewController}).first
            self.navigationController?.popToViewController(vc!, animated: true)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
        
    }
    
    // MARK: - Button Clicked
    @IBAction func btnResetClicked(_ sender: Any) {
        self.view.endEditing(true)
        if validate() {
            resetPasswrdAPICall()
        }
    }
    
    // MARK: - Helper
    func validate() -> Bool
    {
        if txtFieldNewPassword.text!.count < 6 {
            VIEWMANAGER.showToast(message: "Please enter valid 6 digit new password")
            return false
        }
        
        if txtFieldConfirmPass.text!.count == 0 {
            VIEWMANAGER.showToast(message: "Please confirm 6 digit password")
            return false
        }
        if txtFieldConfirmPass.text! != txtFieldNewPassword.text! {
            VIEWMANAGER.showToast(message: "Password not matched.")
            return false
        }
        return true
    }
    
}
