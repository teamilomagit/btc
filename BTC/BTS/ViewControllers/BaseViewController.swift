//
//  BaseViewController.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    @IBOutlet weak var viewNavigation: UIView!
    
    @IBOutlet weak var headerView: AppHeaderView!
    
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!

//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        if self.headerView != nil
//        {
//            self.headerView.stopAnimation()
//        }
//    }
//
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if VIEWMANAGER.currentUser.bTCSubrUserID != nil {
            VIEWMANAGER.getBTCRateAPICall {
                if self.headerView != nil
                {
                    self.headerView.lblBtcRate.text = "1 BTC = \(String(describing: VIEWMANAGER.rateModel.btcRate!)) INR"
                    if VIEWMANAGER.rateModel.btcAccBalance != nil
                    {
                        self.headerView.lblAccBalance.text = "\(String(describing: VIEWMANAGER.rateModel.btcAccBalance!)) BTC"
                    }
                    
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.headerView != nil
        {
            self.headerView.startAnimation()
        }
        self.view.backgroundColor = .white
        if viewNavigation != nil {
            viewNavigation.backgroundColor = UIColor.appThemeColor
            lblNavTitle.textColor = .white
            lblNavTitle.font = UIFont.appBoldFont(size: 18)
        }
        
        if VIEWMANAGER.rateModel != nil && headerView != nil {
           headerView.lblBtcRate.text = "1 BTC = \(String(describing: VIEWMANAGER.rateModel.btcRate!)) INR"
            headerView.lblAccBalance.text = "\(String(describing: VIEWMANAGER.rateModel.btcAccBalance!)) BTC"
        }
    }

    
   
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
