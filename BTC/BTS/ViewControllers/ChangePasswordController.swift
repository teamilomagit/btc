//
//  ChangePasswordController.swift
//  BTS
//
//  Created by Pawan Ramteke on 25/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class ChangePasswordController: BaseViewController {

    @IBOutlet weak var txtFieldMobileNo: MaterialTextField!
    @IBOutlet weak var txtFieldOldPin: MaterialTextField!
    @IBOutlet weak var txtFieldNewPin: MaterialTextField!
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtFieldOldPin.setTextFieldTag(tag: .TAG_PIN_NUMBER)
        txtFieldNewPin.setTextFieldTag(tag: .TAG_PIN_NUMBER)
        
        txtFieldMobileNo.text = "\(VIEWMANAGER.currentUser.mobile!)"
    }
    
    //MARK: - API Call
    
    func changePasswordAPICall()
    {
        let param = [
            "user_id" : VIEWMANAGER.currentUser.bTCSubrUserID,
            "txtPhoneNumber" : txtFieldMobileNo.text!,
            "txtOldPassword":txtFieldOldPin.text!,
            "txtNewPassword":txtFieldNewPin.text!
            ] as [String : AnyObject]
        
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: RESET_PASSWORD_API, params: param, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: response as! String)
            self.navigationController?.popToRootViewController(animated: true)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(message: errMsg!)
        }
    }
    
    //MARK: - Button Clicked
    @IBAction func btnChangePassClicked(_ sender: Any) {
        self.view.endEditing(true)
        if validate() {
            changePasswordAPICall()
        }
    }
    
    //MARK: - Helper
    func validate() -> Bool
    {
        if txtFieldOldPin.text!.count < 6 {
            VIEWMANAGER.showToast(message: "Please enter valid old 6 digit pin")
            return false
        }
        
        if txtFieldNewPin.text!.count < 6 {
            VIEWMANAGER.showToast(message: "Please enter valid new 6 digit pin")
            return false
        }
        return true
    }
    
}
