//
//  MyWalletCell.swift
//  BTS
//
//  Created by Pawan Ramteke on 28/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class MyWalletCell: UITableViewCell {

    @IBOutlet weak var lblSrNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblBuySell: UILabel!
    @IBOutlet weak var lblBtc: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    var deleteClosure : ((IndexPath) -> ())?
    var indexPath : IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnDelete.addTarget(self, action: #selector(btnDeleteClicked), for: .touchUpInside)
    }
    
    @objc func btnDeleteClicked()
    {
        if (deleteClosure != nil) {
            deleteClosure!(indexPath)
        }
    }
    
    func onDeleteClicked(closure:@escaping (IndexPath) -> ())
    {
        deleteClosure = closure
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
