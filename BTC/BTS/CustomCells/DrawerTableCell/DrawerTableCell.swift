//
//  DrawerTableCell.swift
//  BTS
//
//  Created by Pawan Ramteke on 18/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class DrawerTableCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = UIFont.appRegularFont(size: 16)
        lblTitle.textColor = UIColor.darkGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
