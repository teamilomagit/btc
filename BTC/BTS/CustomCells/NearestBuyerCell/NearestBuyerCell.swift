//
//  NearestBuyerCell.swift
//  BTS
//
//  Created by Pawan Ramteke on 22/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class NearestBuyerCell: UITableViewCell {

    @IBOutlet weak var lblSrNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblBtc: UILabel!
    @IBOutlet weak var btnDetails: UIButton!
    
    var indexPath : IndexPath!
    
    var detailsClosure : ((IndexPath) -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnDetails.setImage(UIImage(named:"ic_info")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnDetails.tintColor = UIColor.appThemeColor
        btnDetails.addTarget(self, action: #selector(btnDetailsClicked), for: .touchUpInside)
    }
    
    @objc func btnDetailsClicked()
    {
        if detailsClosure != nil {
            detailsClosure!(indexPath)
        }
    }
    
    func onInfoButtonClicked(closure:@escaping ((IndexPath) -> ()))
    {
        detailsClosure = closure
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
