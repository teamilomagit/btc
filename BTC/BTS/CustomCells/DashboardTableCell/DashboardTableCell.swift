//
//  DashboardTableCell.swift
//  BTS
//
//  Created by Pawan Ramteke on 18/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class DashboardTableCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        lblRate.font = UIFont.appRegularFont(size: 13)
        lblDate.font =  UIFont.appRegularFont(size: 13)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
