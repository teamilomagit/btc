//
//  AccountStmtCell.swift
//  BTS
//
//  Created by Pawan Ramteke on 22/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class AccountStmtCell: UITableViewCell {

    @IBOutlet weak var lblSrNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var btnInfo: UIButton!
    
    var infoClosure:((IndexPath) -> ())?
    var indexPath:IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnInfo.setImage(UIImage(named:"ic_info")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnInfo.tintColor = UIColor.appThemeColor
        btnInfo.addTarget(self, action: #selector(btnInfoClicked), for: .touchUpInside)
    }
    
    func onInfoButtonClicked(closure:@escaping ((IndexPath) -> ()))
    {
        infoClosure = closure
    }
    
    @objc func btnInfoClicked()
    {
        if infoClosure != nil {
            infoClosure!(indexPath)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
