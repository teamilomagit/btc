//
//  MaterialTextField.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField


enum TextFieldTag : Int {
    case TAG_MOBILE = 100
    case TAG_DATE_PICKER = 101
    case TAG_ACTION_SHEET = 102
    case TAG_DROP_DOWN   = 103
    case TAG_PIN_NUMBER = 104
    case TAG_BTC_VALUE = 105

}


class MaterialTextField: UITextField,UITextFieldDelegate {
    
    var onDataSelect : ((String)->())?

    var actionSheetData : NSArray!
    var dropDownData : [String]!
    var separator : UIView!
    var isHideLine : Bool = false
     @IBInspectable var leftImage : String = ""  {
        didSet{
            self.leftViewMode = .always
            let imgView = UIImageView(frame: CGRect(x: 0, y:0, width: 40, height: 40))
            imgView.image = UIImage(named: leftImage)?.withRenderingMode(.alwaysTemplate)
            imgView.tintColor = UIColor.appThemeColor
            imgView.contentMode = .center
            self.leftView = imgView
        }
    }
    
    @IBInspectable var hideLine : Bool = true {
        didSet {
            self.isHideLine  = hideLine
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    convenience init() {
        self.init()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup()
    {
        separator = UIView()
        separator.backgroundColor = .gray
        separator.isHidden = self.isHideLine
        self.addSubview(separator)
        separator.enableAutoLayout()
        separator.bottomMargin(pixels: 0)
        separator.leadingMargin(pixels: 0)
        separator.trailingMargin(pixels: 0)
        separator.fixedHeight(pixels: 1)
        
     //   self.floatingLabelFont = .appRegularFont(size: 16)
     //   self.floatingLabelActiveTextColor = .appThemeColor
      
        self.delegate = self
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == TextFieldTag.TAG_DATE_PICKER.rawValue {
            showDatePicker()
            return false
        }
        if textField.tag == TextFieldTag.TAG_ACTION_SHEET.rawValue {
            showActionSheet()
            return false
        }
        if textField.tag == TextFieldTag.TAG_DROP_DOWN.rawValue {
            showDropDown()
            return false
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,let textRange = Range(range, in: text)
        {
            let updatedText = text.replacingCharacters(in: textRange,with: string)
            
            if self.tag == TextFieldTag.TAG_MOBILE.rawValue
            {
                return updatedText.count <= 12
            }
            
            if self.tag == TextFieldTag.TAG_PIN_NUMBER.rawValue
            {
                return updatedText.count <= 6
            }
            if self.tag == TextFieldTag.TAG_BTC_VALUE.rawValue
            {
                return updatedText.count <= 4
            }
        }
        return true
    }
    
    func setTextFieldTag(tag:TextFieldTag)
    {
        self.tag = tag.rawValue
        if tag == .TAG_MOBILE{
            self.keyboardType = .phonePad
        }
        if tag == .TAG_PIN_NUMBER {
            self.keyboardType = .phonePad
            self.isSecureTextEntry = true
        }
        if tag == .TAG_ACTION_SHEET || tag == .TAG_DROP_DOWN {
            self.rightViewMode = .always
            let imgView = UIImageView(frame: CGRect(x: 0, y:0, width: 20, height: 20))
            imgView.image = UIImage(named: "ic_dropdown")?.withRenderingMode(.alwaysTemplate)
            imgView.tintColor = UIColor.appThemeColor
            imgView.contentMode = .center
            self.rightView = imgView
        }
    }
    
    func showDatePicker()
    {
        let datePicker = DatePickerView(frame: UIScreen.main.bounds)
        VIEWMANAGER.topMostController().navigationController?.view.addSubview(datePicker)
        datePicker.onDoneClicked { (date) in
            self.text = date
        }
    }
    
    func showActionSheet()
    {
        if actionSheetData == nil {
            return
        }
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        for i in actionSheetData {
            let alertAction = UIAlertAction(title: i as? String, style: .default, handler: { (action) in
                self.text = action.title
                if self.onDataSelect != nil{
                    self.onDataSelect!(action.title!)
                }
            })
            alert.addAction(alertAction)
            
        }
        VIEWMANAGER.topMostController().present(alert, animated: true, completion: nil)
    }
    
    func showDropDown()
    {
        if dropDownData == nil{
            return
        }
        
        let dropDown = DropDownListView(frame: UIScreen.main.bounds, title: self.placeholder!,data:dropDownData)
        UIApplication.shared.keyWindow?.addSubview(dropDown)
        dropDown.onDoneClicked { (data) in
            self.text = data
            if self.onDataSelect != nil{
                self.onDataSelect!(data)
            }
        }
    }
    
    func onDataSelectionSuccess(success:@escaping (String)->()){
        onDataSelect = success
    }
}
