//
//  AppHeaderView.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit


class AppHeaderView: UIView {
    
    var backClosure : (()->())?
    
    var gradient = CAGradientLayer()
    var btnBack : UIButton!
    var lblHeaderTitle : UILabel!
    var lblBtcRate : UILabel!
    var lblAccBalance : UILabel!
    var btnCenterLogo : UIButton!
    var lblMyWallet : UILabel!
    var timer : Timer!
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    func setup()
    {
        gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [UIColor.white.withAlphaComponent(1.0).cgColor,UIColor.appThemeColor.cgColor]
        self.layer.insertSublayer(gradient, at: 0)
        
        let header = UIView()
        header.backgroundColor = .appThemeColor
        self.addSubview(header)
        header.enableAutoLayout()
        header.leadingMargin(pixels: 10)
        header.topMargin(pixels: 40)
        header.trailingMargin(pixels: 10)
        header.fixedHeight(pixels: 40)
        
        btnBack = UIButton()
        btnBack.setImage(UIImage(named:"back"), for: .normal)
        btnBack.addTarget(self, action: #selector(AppHeaderView.btnBackClicked), for: .touchUpInside)
        header.addSubview(btnBack)
        btnBack.enableAutoLayout()
        btnBack.leadingMargin(pixels: 0);
        btnBack.topMargin(pixels: 0)
        btnBack.bottomMargin(pixels: 0)
        btnBack.fixedWidth(pixels: 40)
        
        lblHeaderTitle = UILabel()
        lblHeaderTitle.font = UIFont.appBoldFont(size: 18)
        lblHeaderTitle.textColor = .white
        header.addSubview(lblHeaderTitle)
        lblHeaderTitle.enableAutoLayout()
        lblHeaderTitle.addToRightToView(view: btnBack, pixels: 10)
        lblHeaderTitle.trailingMargin(pixels: 10)
        lblHeaderTitle.topMargin(pixels: 0)
        lblHeaderTitle.bottomMargin(pixels: 0)
        
        btnCenterLogo = UIButton()
        btnCenterLogo.contentMode = .scaleAspectFit
        btnCenterLogo.setImage(UIImage(named:"login_logo"), for: .normal)
        btnCenterLogo.addTarget(self, action: #selector(AppHeaderView.btnCenterLogoClicked), for: .touchUpInside)
        self.addSubview(btnCenterLogo)
        btnCenterLogo.enableAutoLayout()
        btnCenterLogo.centerX()
        btnCenterLogo.belowToView(view: header, pixels: 10)
        btnCenterLogo.fixedWidth(pixels: 60)
        btnCenterLogo.fixedHeight(pixels: 60)
        
        lblMyWallet = UILabel()
        lblMyWallet.font = UIFont.appRegularFont(size: 15)
        lblMyWallet.textAlignment = .center
        lblMyWallet.textColor = .darkText
        lblMyWallet.text = "My Wallet"
        self.addSubview(lblMyWallet)
        lblMyWallet.enableAutoLayout()
        lblMyWallet.centerX(toView: btnCenterLogo, pixels: 0)
        lblMyWallet.belowToView(view: btnCenterLogo, pixels: 5)
        
        let viewBTCRate = UIView()
        self.addSubview(viewBTCRate)
        viewBTCRate.enableAutoLayout()
        viewBTCRate.leadingMargin(pixels: 0)
        viewBTCRate.addToLeftToView(view: btnCenterLogo, pixels: 0)
        viewBTCRate.centerY(toView: btnCenterLogo, pixels: 0.0)
        
        let lblBtcRateTitle = UILabel()
        lblBtcRateTitle.text = "Today's BTC Rate"
        lblBtcRateTitle.font = UIFont.appRegularFont(size: 14)
        lblBtcRateTitle.adjustsFontSizeToFitWidth = true
        lblBtcRateTitle.textAlignment = .center
        viewBTCRate.addSubview(lblBtcRateTitle);
        lblBtcRateTitle.enableAutoLayout()
        lblBtcRateTitle.leadingMargin(pixels: 0)
        lblBtcRateTitle.trailingMargin(pixels: 0)
        lblBtcRateTitle.topMargin(pixels: 0)
        
        lblBtcRate = UILabel()
        lblBtcRate.text = "-"
        lblBtcRate.font = UIFont.appBoldFont(size: 18)
        lblBtcRate.adjustsFontSizeToFitWidth = true
        lblBtcRate.textAlignment = .center
        viewBTCRate.addSubview(lblBtcRate);
        lblBtcRate.enableAutoLayout()
        lblBtcRate.leadingMargin(pixels: 0)
        lblBtcRate.trailingMargin(pixels: 0)
        lblBtcRate.belowToView(view: lblBtcRateTitle, pixels: 5)
        lblBtcRate.bottomMargin(pixels: 0)
        
        let viewAccBalance = UIView()
        self.addSubview(viewAccBalance)
        viewAccBalance.enableAutoLayout()
        viewAccBalance.trailingMargin(pixels: 0)
        viewAccBalance.addToRightToView(view: btnCenterLogo, pixels: 0)
        viewAccBalance.centerY(toView: btnCenterLogo, pixels: 0.0)
        
        let lblAccBalanceTitle = UILabel()
        lblAccBalanceTitle.text = "Your Account Balance"
        lblAccBalanceTitle.font = UIFont.appRegularFont(size: 14)
        lblAccBalanceTitle.adjustsFontSizeToFitWidth = true
        lblAccBalanceTitle.textAlignment = .center
        viewAccBalance.addSubview(lblAccBalanceTitle);
        lblAccBalanceTitle.enableAutoLayout()
        lblAccBalanceTitle.leadingMargin(pixels: 0)
        lblAccBalanceTitle.trailingMargin(pixels: 0)
        lblAccBalanceTitle.topMargin(pixels: 0)
        
        lblAccBalance = UILabel()
        lblAccBalance.text = "-"
        lblAccBalance.font = UIFont.appBoldFont(size: 18)
        lblAccBalance.adjustsFontSizeToFitWidth = true
        lblAccBalance.textAlignment = .center
        viewAccBalance.addSubview(lblAccBalance);
        lblAccBalance.enableAutoLayout()
        lblAccBalance.leadingMargin(pixels: 0)
        lblAccBalance.trailingMargin(pixels: 0)
        lblAccBalance.belowToView(view: lblAccBalanceTitle, pixels: 5)
        lblAccBalance.bottomMargin(pixels: 0)
        
       // aniamte()
    }
    
    
    func startAnimation()
    {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(AppHeaderView.timerEvent), userInfo: nil, repeats: false)
    }
    
    @objc func timerEvent()
    {
        UIView.animate(withDuration: 1.0, delay: 0, options: [.allowUserInteraction], animations: {
            self.btnCenterLogo.alpha = 0.1
            self.lblMyWallet.alpha = 0.1
        }) { (stats) in
            UIView.animate(withDuration: 1.0, delay: 0, options: [.allowUserInteraction], animations: {
                self.btnCenterLogo.alpha = 1.0
                self.lblMyWallet.alpha = 1.0
            }) { (stats) in
                self.timerEvent()
            }
        }
    }
    
    func stopAnimation()
    {
        timer.invalidate()
        timer = nil
    }
    
    func aniamte()
    {
        UIView.animate(withDuration: 1, delay: 0, options: [.repeat,.autoreverse,.allowUserInteraction], animations: {
            self.btnCenterLogo.alpha = 0.1
            self.lblMyWallet.alpha = 0.1
        }, completion: nil)
    }
    @objc func btnBackClicked()
    {
        if backClosure != nil {
            backClosure!()
            return
        }
        VIEWMANAGER.topMostController().navigationController?.popViewController(animated: true)
    }
    
    @objc func btnCenterLogoClicked()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myWalletVC = storyboard.instantiateViewController(withIdentifier:"MyBTCWalletController")
        VIEWMANAGER.topMostController().navigationController?.pushViewController(myWalletVC, animated: true)
    }
    
    func onBackClicked(back : @escaping () -> Void)
    {
        backClosure = back
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
      //  self.frame.size.height = 190
        
        gradient.frame = self.bounds
    }

}
