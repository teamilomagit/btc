//
//  DropDownListView.swift
//  BTS
//
//  Created by Pawan Ramteke on 01/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class DropDownListView: UIView,UITableViewDataSource,UITableViewDelegate {

    var doneClosure : ((String)->())?

    var baseView : UIView!
    var tblDataArr : [String]!
    let BASE_HEIGHT = (SCREEN_HEIGHT * 0.5)
    init(frame: CGRect,title: String, data:[String]) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.alpha = 0
        
        tblDataArr = data
        
        let btnBG = UIButton(frame: self.frame)
        btnBG.addTarget(self, action: #selector(DropDownListView.btnCloseClicked), for: .touchUpInside)
        self.addSubview(btnBG)
        
        baseView = UIView(frame: CGRect(x: 10, y: self.frame.midY - BASE_HEIGHT/2, width: self.frame.size.width - 20, height: BASE_HEIGHT))
        baseView.backgroundColor = .white
        self.addSubview(baseView)
        
        let lblTitle = UILabel(frame: CGRect(x: 0, y: 0, width: baseView.frame.size.width, height: 50))
        lblTitle.backgroundColor = UIColor.appThemeColor
        baseView.addSubview(lblTitle)
        lblTitle.font = UIFont.appBoldFont(size: 20)
        lblTitle.textAlignment = .center
        lblTitle.text = title
        lblTitle.textColor = .white
        
        let tblView = UITableView(frame: CGRect(x: 0, y: lblTitle.frame.maxY, width: baseView.frame.size.width, height: baseView.frame.size.height - lblTitle.frame.size.height))
        tblView.dataSource = self
        tblView.delegate = self
        baseView.addSubview(tblView)
        
        UIView.animate(withDuration: 0.2) {
            self.alpha = 1;
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CellId")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "CellId")
        }
        cell?.textLabel?.font = UIFont.appRegularFont(size: 18)
        cell?.textLabel?.text = tblDataArr[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if doneClosure != nil {
            doneClosure!(tblDataArr[indexPath.row])
        }
        btnCloseClicked()
    }
    
    @objc func btnCloseClicked()
    {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0
        }) { (completion) in
            self.removeFromSuperview()
        }
    }
    
    func onDoneClicked(back : @escaping (String) -> Void)
    {
        doneClosure = back
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
