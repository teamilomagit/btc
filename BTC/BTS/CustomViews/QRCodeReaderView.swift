//
//  QRCodeReaderView.swift
//  BTS
//
//  Created by Pawan Ramteke on 26/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import AVFoundation
class QRCodeReaderView: UIView,AVCaptureMetadataOutputObjectsDelegate{
    var captureSession : AVCaptureSession!
    var videoPreviewLayer : AVCaptureVideoPreviewLayer!

    var readSuccessClosure : ((String)->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func startReading()
    {
        if captureSession != nil {
            DispatchQueue.main.async {
                self.captureSession.startRunning()
            }
            return
        }
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            captureSession = AVCaptureSession()
            captureSession.addInput(input)
            
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue(label: "myQueue"))
            captureMetadataOutput.metadataObjectTypes = [.qr,.upce,.code39,.code39Mod43,.code93,.code128,.ean8,.ean13,.aztec,.pdf417,.itf14,.interleaved2of5,.dataMatrix]
            
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer.videoGravity = .resizeAspectFill
            videoPreviewLayer.frame = self.layer.bounds
            self.layer.addSublayer(videoPreviewLayer)

            let transperentView = UIView(frame: self.bounds)
            transperentView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            self.addSubview(transperentView)
            
            let bounds = transperentView.bounds
            let maskLayer = CAShapeLayer()
            maskLayer.frame = bounds
            maskLayer.fillColor = UIColor.black.cgColor
            
            let kRadius : CGFloat = SCREEN_WIDTH * 0.8;
            
            let circleRect = CGRect(x: bounds.midX - (kRadius/2),y: bounds.midY - (kRadius/2),width: kRadius,height: kRadius);
            let path = UIBezierPath(roundedRect: circleRect, cornerRadius: 0)
            path.append(UIBezierPath(rect: bounds))
            maskLayer.path = path.cgPath
            maskLayer.fillRule = kCAFillRuleEvenOdd
            
            transperentView.layer.mask = maskLayer;
            
            
            let lblTitle = UILabel(frame: CGRect(x: 20, y: 10, width: transperentView.frame.size.width - 40, height: 40))
            lblTitle.font = UIFont.appBoldFont(size: 20)
            lblTitle.text = "Scan QR Code"
            lblTitle.textColor = .white
            lblTitle.textAlignment = .center
            transperentView.addSubview(lblTitle)

            let lblDescr = UILabel(frame: CGRect(x: 10, y: transperentView.frame.maxY - 50, width: transperentView.frame.size.width - 20, height: 50))
            lblDescr.font = UIFont.appRegularFont(size: 14)
            lblDescr.text = "Place barcode inside the viewfinder rectangle to scan"
            lblDescr.numberOfLines = 0
            lblDescr.textColor = .white
            transperentView.addSubview(lblDescr)
            
            captureSession.startRunning()
            
        }
        catch {
            if !isCameraPermissionActive() {
                CustomAlertView.showPermissionEnableAlert(title: "Camera Disabled", message: "Access to the camera has been prohibited, please enable it in the setting app to continue.", completion: {
                    UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                })
            }
        }
        
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count > 0 {
            let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
            if metadataObj.type == .qr
            {
                DispatchQueue.main.async {
                    self.captureSession.stopRunning()
                }
                
                if readSuccessClosure != nil {
                    readSuccessClosure!(metadataObj.stringValue!)
                }
                //lblData.text = metadataObj.stringValue
            }
            
        }
    }
    
    func onQRReadSuccess(back : @escaping (String) -> Void)
    {
        readSuccessClosure = back
    }
    
    func isCameraPermissionActive() -> Bool
    {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .denied:
            return false
        case .authorized:
            return true
            
        case .restricted:
            return false
            
        case .notDetermined:
           return false
        }
    }

}
