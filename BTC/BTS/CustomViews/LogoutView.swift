//
//  LogoutView.swift
//  BTS
//
//  Created by Pawan Ramteke on 26/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class LogoutView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.alpha = 0
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let baseView = MaterialView(frame: CGRect(x: 20, y: self.frame.midY - 100, width: self.frame.size.width - 40, height: 200))
        self.addSubview(baseView)
        
        let lblTitle = UILabel(frame: CGRect(x: 20, y: 20, width: baseView.frame.size.width - 40, height: 50))
        lblTitle.font = UIFont.appBoldFont(size: 20)
        lblTitle.textAlignment = .center
        lblTitle.numberOfLines = 0
        lblTitle.text = "Are you sure you want to logout?"
        baseView.addSubview(lblTitle)
        //lblTitle.sizeToFit()
        
        let btnYes = UIButton(frame: CGRect(x: 20, y: lblTitle.frame.maxY + 40, width: baseView.frame.size.width / 2 - 25, height: 40))
        btnYes.backgroundColor = UIColor.controllerBGColor
        btnYes.titleLabel?.font = UIFont.appBoldFont(size: 16)
        btnYes.setTitle("Yes", for: .normal)
        btnYes.setTitleColor(.black, for: .normal)
        btnYes.addTarget(self, action: #selector(LogoutView.btnYesClicked), for: .touchUpInside)

        baseView.addSubview(btnYes)
        
        let btnNo = UIButton(frame: CGRect(x: btnYes.frame.maxX + 10, y: btnYes.frame.minY, width: btnYes.frame.size.width, height: btnYes.frame.size.height))
        btnNo.backgroundColor = UIColor.appThemeColor
        btnNo.titleLabel?.font = UIFont.appBoldFont(size: 16)
        btnNo.setTitle("No", for: .normal)
        btnNo.setTitleColor(.white, for: .normal)
        btnNo.addTarget(self, action: #selector(LogoutView.btnNoClicked), for: .touchUpInside)
        baseView.addSubview(btnNo)
        
        
        UIView.animate(withDuration: 0.4) {
            self.alpha = 1
        }
    }
    
    @objc func btnNoClicked()
    {
        UIView.animate(withDuration: 0.4, animations: {
            self.alpha = 0
        }) { (status) in
            self.removeFromSuperview()
        }
    }
    
    @objc func btnYesClicked()
    {
        Preferences.clearLoginData()
        let app_del = UIApplication.shared.delegate as! AppDelegate
        app_del.setRegistrationAsRoot()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
