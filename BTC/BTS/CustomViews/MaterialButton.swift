//
//  MaterialButton.swift
//  BTS
//
//  Created by Pawan Ramteke on 17/11/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class MaterialButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    convenience init() {
        self.init()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup()
    {
        self.backgroundColor = .appThemeColor
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 2
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.layer.cornerRadius = 2;
        self.setTitleColor(.white, for: .normal)
        self.titleLabel?.font = UIFont.appBoldFont(size: 18)
    }
    
    
}
